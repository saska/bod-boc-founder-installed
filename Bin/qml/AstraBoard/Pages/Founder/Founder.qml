import QtQuick 2.0
import AstraMuseum 1.0
import "../../Components/Circle"
import "../../Components/Utils"
import "../../Components/TitleText"
import "../../Components/TextBoxCanvas"

Item {
    id: root

    property ListModel listModel
    property ListModel currentPeople : ListModel{}
    property string locale : currentLocale
    property var title : currentTitle

    property int selectedIndex: 0
    property double lowestX1
    property double lowestX2
    property double lowestY

    property url personLeftImage : currentPeople.count > 1 ? currentPeople.get(1).image : ""
    property url personRigthImage : currentPeople.count > 0 ? currentPeople.get(0).image : ""
    property string personLeftName : currentPeople.count > 1 ? currentPeople.get(1).name : ""
    property string personRigthName : currentPeople.count > 0 ? currentPeople.get(0).name : ""

    state: "B"

    states: [
        State {
            name: "A"
        },
        State {
            name: "B"
        },
        State {
            name: "SELECTED1"
        },
        State {
            name: "SELECTED2"
        },
        State{
            name: "TEMP"
        }
    ]

    transitions: [
        Transition {
            from: "B"
            to: "A"
            SequentialAnimation{
                ParallelAnimation{
                    NumberAnimation{ target: motionPath1; property: "progress"; from: 0.3; to: 0.15; easing.type: Easing.InOutSine; duration: 9000}
                    NumberAnimation{ target: motionPath2; property: "progress"; from: 0.85; to: 0.7; easing.type: Easing.InOutSine; duration: 9000}
                    NumberAnimation{ target: itemLeft; property: "scale";  to: 1.0; easing.type: Easing.Linear; duration: 8500}
                    NumberAnimation{ target: itemRight; property: "scale";  to: 0.8; easing.type: Easing.Linear; duration: 8500}
                }

                /*ParallelAnimation{
                    NumberAnimation{ target: motionPath1; property: "progress"; to: 0.15; easing.type: Easing.OutSine; duration: 1000}
                    NumberAnimation{ target: motionPath2; property: "progress"; to: 0.7; easing.type: Easing.OutSine; duration: 1000}
                }*/

                PropertyAction{ target: root; property: "state"; value: "B"}
            }
        },
        Transition {
            from: "A"
            to: "B"
            SequentialAnimation{
                ParallelAnimation{
                    NumberAnimation{ target: motionPath1; property: "progress"; from: 0.15; to: 0.30; easing.type: Easing.InOutSine; duration: 9000}
                    NumberAnimation{ target: motionPath2; property: "progress"; from: 0.7; to: 0.85; easing.type: Easing.InOutSine; duration: 9000}
                    NumberAnimation{ target: itemLeft; property: "scale";  to: 0.8; easing.type: Easing.Linear; duration: 8500}
                    NumberAnimation{ target: itemRight; property: "scale";  to: 1.0; easing.type: Easing.Linear; duration: 8500}
                }

                PropertyAction{ target: root; property: "state"; value: "A"}
            }
        },
        Transition {
            from: "*"
            to: "SELECTED1"
            SequentialAnimation{

                ParallelAnimation{
                    NumberAnimation{ target: itemRight; property: "opacity"; to: 0.0; duration: 500}
                    NumberAnimation{ target: title; property: "opacity"; to: 0.0; duration: 500}
                }

                ParallelAnimation{
                    NumberAnimation{ target: itemLeft; property: "y"; to: root.height/2; easing.type: Easing.OutSine; duration: 500}
                    NumberAnimation{ target: itemLeft; property: "x"; to: root.width/2 - 285; easing.type: Easing.OutSine; duration: 500}
                    NumberAnimation{ target: itemLeft; property: "scale"; to: 1.0; easing.type: Easing.OutSine; duration: 500}
                }

                ParallelAnimation{
                    NumberAnimation{ target: motionPath2; property: "progress"; to: 0.7; easing.type: Easing.OutSine; duration: 200}
                    NumberAnimation{ target: itemRight; property: "scale"; to: 0.8; easing.type: Easing.OutSine; duration: 200}
                }

                PropertyAction { target: itemLeft; property: "stripsVisible"; value: true }
                PauseAnimation { duration: 500 }
                PropertyAction { target: textBox; property: "isLeft"; value: true }
                PropertyAction { target: textBox; property: "visible"; value: true }
                PropertyAction{ target: root; property: "selectedIndex"; value: 1}
                PropertyAction{ target: root; property: "state"; value: "A"}
            }
        },

        Transition {
            from: "*"
            to: "SELECTED2"

            SequentialAnimation{

                ParallelAnimation{
                    NumberAnimation{ target: itemLeft; property: "opacity"; to: 0.0; duration: 500}
                    NumberAnimation{ target: title; property: "opacity"; to: 0.0; duration: 500}
                }

                ParallelAnimation{
                    NumberAnimation{ target: itemRight; property: "y"; to: root.height/2 ; easing.type: Easing.OutSine; duration: 500}
                    NumberAnimation{ target: itemRight; property: "x"; to: root.width/2 + 285; easing.type: Easing.OutSine; duration: 500}
                    NumberAnimation{ target: itemRight; property: "scale"; to: 1.0; easing.type: Easing.OutSine; duration: 500}
                }

                ParallelAnimation{
                    NumberAnimation{ target: motionPath1; property: "progress"; to: 0.3; easing.type: Easing.OutSine; duration: 200}
                    NumberAnimation{ target: itemLeft; property: "scale"; to: 0.8; easing.type: Easing.OutSine; duration: 200}
                }

                PropertyAction { target: itemRight; property: "stripsVisible"; value: true }
                PauseAnimation { duration: 500 }
                PropertyAction { target: textBox; property: "isLeft"; value: false }
                PropertyAction { target: textBox; property: "visible"; value: true }

                PropertyAction{ target: root; property: "selectedIndex"; value: 2}
                PropertyAction{ target: root; property: "state"; value: "B"}
            }
        },

        Transition {
            from: "TEMP"
            to: "A"

            SequentialAnimation{

                PropertyAction { target: itemLeft; property: "stripsVisible"; value: false }
                PauseAnimation { duration: 500 }

                ParallelAnimation{
                    NumberAnimation{ target: itemLeft; property: "y"; to: root.lowestY; easing.type: Easing.OutSine; duration: 1000}
                    NumberAnimation{ target: itemLeft; property: "x"; to: root.lowestX1; easing.type: Easing.OutSine; duration: 1000}
                }

                ParallelAnimation{
                    NumberAnimation{ target: title; property: "opacity"; to: 1.0; duration: 500}
                    NumberAnimation{ target: itemRight; property: "opacity"; to: 1.0; duration: 500}
                }

                PropertyAction{ target: root; property: "state"; value: "B"}
            }
        },
        Transition {
            from: "TEMP"
            to: "B"

            SequentialAnimation{

                PropertyAction { target: itemRight; property: "stripsVisible"; value: false }
                PauseAnimation { duration: 500 }

                ParallelAnimation{
                    NumberAnimation{ target: itemRight; property: "y"; to: root.lowestY; easing.type: Easing.OutSine; duration: 1000}
                    NumberAnimation{ target: itemRight; property: "x"; to: root.lowestX2; easing.type: Easing.OutSine; duration: 1000}
                }

                ParallelAnimation{
                    NumberAnimation{ target: title; property: "opacity"; to: 1.0; duration: 500}
                    NumberAnimation{ target: itemLeft; property: "opacity"; to: 1.0; duration: 500}
                }
                PropertyAction{ target: root; property: "state"; value: "A"}
            }
        }
    ]

    FounderCircle{
        id: itemLeft
        source: root.personLeftImage
        x : motionPath1.x
        y : motionPath1.y
        onClicked: {
            if(root.selectedIndex == 0){
                itemLeft.smoothing = false
                itemRight.smoothing = false
                root.selectedIndex = 1
                root.state = "SELECTED1"
                Content.readText("0", 0, root.locale, root.personLeftName);
                localeSwitch.hide()
            }
        }
    }

    FounderCircle{
        id: itemRight
        source: root.personRigthImage
        x : motionPath2.x
        y : motionPath2.y
        onClicked: {
            if(root.selectedIndex == 0){
                itemLeft.smoothing = false
                itemRight.smoothing = false
                root.selectedIndex = 2
                root.state = "SELECTED2"
                Content.readText("0", 0, root.locale, root.personRigthName);
                localeSwitch.hide()
            }
        }
    }

    PathInterpolator {
        id: motionPath1

        path: EllipsePath{
            width: root.width
            height: root.height
            margin: 300
        }
    }

    PathInterpolator {
        id: motionPath2

        path: EllipsePath{
            width: root.width
            height: root.height
            margin: 300
        }
    }

    function reset(){
        if(root.state == "A" || root.state == "B") {
            if(root.selectedIndex == 1){
                root.state = "TEMP"
                root.state = "A"

            }else if(root.selectedIndex == 2){
                root.state = "TEMP"
                root.state = "B"
            }
        }
        textBox.visible = false
        root.selectedIndex = 0;
    }

    function init(){

        motionPath1.progress = 0.15
        motionPath2.progress = 0.7
        root.lowestX1 = itemLeft.x

        motionPath1.progress = 0.3
        motionPath2.progress = 0.85
        itemLeft.scale = 0.8
        itemRight.scale = 1.0

        root.lowestY = itemRight.y
        root.lowestX2 = itemRight.x

        root.state = "A"
    }

    Connections{
        target: loadingItem
        onHidden: {
            init()
            localeSwitch.setCenter()
        }
    }

    visible: !loadingItem.loading

    TitleText{
        id: title
        anchors.centerIn: parent
        fontColor: "white"
        fontPixelSize: 80
        text: founderBox.title[founderBox.locale]
        visible: opacity > 0
        active: !textBox.isOpen
    }

    // Text
    TextBoxCanvas{
        id: textBox
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: textBox.isLeft ? 224 : -224
        visible: false
        onClosed: {
            root.reset()
            delayLocaleSwitchShow.start()
        }
        onClosing: {
            localeSwitch.hide()
        }
        onFinished: {
            if(textBox.isOpen){
                localeSwitch.show()
                localeSwitch.active = false
            }
        }

        onLoadLocale: {
            if(root.selectedIndex == 1)
                Content.readText("0", 0, root.locale, root.personLeftName);
            else
                Content.readText("0", 0, root.locale, root.personRigthName);
        }

    }

    Connections{
        target: Content
        onReadTextResponse: {
            if(jsonData){
                textBox.titleText = jsonData.title ? jsonData.title : ""
                textBox.subtitleText = jsonData.subtitle ? jsonData.subtitle : ""
                textBox.contentText = jsonData.text ? jsonData.text : ""
            }
        }
    }

    Connections{
        target: localeSwitch
        onIsIDChanged : {
            if(root.selectedIndex > 0 && textBox.isOpen){
                textBox.swicthLocale()
            }
        }
    }

    Timer{
        id: delayLocaleSwitchShow
        interval: 2000
        onTriggered: {
            localeSwitch.show()
            localeSwitch.active = true
        }
    }

    Connections{
        target: idleObject
        onWakeup: {
            if(idleTimeline.running) {
                idleTimeline.stop()
            }
            idleRect.visible = false
        }
        onSleep: {
            if(idleState == "IDLE"){
                if(!idleTimeline.running){
                    checkBeforeIdle()
                    idleRect.visible = false
                    idleTimeline.start()
                }
            }else if(idleState == "IDLE_LONG"){
                idleTimeline.stop()
                idleRect.visible = true
            }
        }
    }

    function checkBeforeIdle(){
        // textBox
        textBox.simulateClose()
    }

    /*
      0. select left or right
      1. wait
      2. flick to down
      3. change locale
      4. flick to up
      5. close
      7. wait
      8. change locale

     */

    property int tick: 0
    property bool idleSelectLeft: true

    SequentialAnimation on tick{
        id: idleTimeline
        running: false
        loops: Animation.Infinite
        alwaysRunToEnd: false

        PauseAnimation { duration: 10000 }
        ScriptAction{ script: {root.checkBeforeIdle()}}
        PauseAnimation { duration: 8000 }
        ScriptAction{ script: {itemLeft.clicked()}}
        PauseAnimation { duration: 5000 }
        ScriptAction{ script: localeSwitch.simulateSwitchLocale()}
        PauseAnimation { duration: 1000 }
        SequentialAnimation{
            loops: 5
            ScriptAction{ script: textBox.simulateFlick()}
            PauseAnimation { duration: 1000 }
        }
        ScriptAction{ script: textBox.simulateClose()}
        PauseAnimation { duration: 8000 }

        ScriptAction{ script: {itemRight.clicked()}}
        PauseAnimation { duration: 5000 }
        ScriptAction{ script: localeSwitch.simulateSwitchLocale()}
        PauseAnimation { duration: 1000 }
        SequentialAnimation{
            loops: 5
            ScriptAction{ script: textBox.simulateFlick()}
            PauseAnimation { duration: 1000 }
        }
        ScriptAction{ script: textBox.simulateClose()}
        PauseAnimation { duration: 8000 }
    }

    Keys.onDigit0Pressed: {
        if(!idleTimeline.running){
            checkBeforeIdle()
            idleRect.visible = false
            idleTimeline.start()
        }
    }

    Component.onCompleted: {
        forceActiveFocus()
    }
}
