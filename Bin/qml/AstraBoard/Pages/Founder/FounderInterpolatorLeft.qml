import QtQuick 2.0
import "../../Components/Utils"

PathInterpolator {
    id: pathAnim

    property Item item
    property int ellipseWidth
    property int ellipseHeight
    property int margin
    property int duration: 10000

    path: EllipsePath{
        width: window.width
        height: window.height
        margin: 300
    }

    SequentialAnimation on progress {
        running: true
        loops: Animation.Infinite

        ParallelAnimation{
            NumberAnimation {
                from: 0.125; to: 0.30
                duration: pathAnim.duration
                easing.type: Easing.Linear
            }

            NumberAnimation { target: item; property: "scale"; from: 1.0; to: 0.8; duration: pathAnim.duration; easing.type: Easing.Linear }
        }

        NumberAnimation{
            to: 0.325
            duration: pathAnim.duration/3
            easing.type: Easing.OutSine
        }

        ParallelAnimation{
            NumberAnimation {
                from: 0.325; to: 0.15
                duration: pathAnim.duration
                easing.type: Easing.Linear
            }
            NumberAnimation { target: item; property: "scale"; from: 0.8; to: 1.0; duration: pathAnim.duration; easing.type: Easing.Linear }
        }

        NumberAnimation{
            to: 0.125
            duration: pathAnim.duration/3
            easing.type: Easing.OutSine
        }
    }
}
