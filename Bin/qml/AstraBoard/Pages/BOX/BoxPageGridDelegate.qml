import QtQuick 2.0
import "../../Components/Circle"

CircleFramed{
    id: circle

    property double theCircleScale : circleScale
    property url theImage : image
    property string theText : nameTag

    scale: circle.theCircleScale
    source: circle.theImage
    text: circle.theText
    anchors.centerIn: parent
}
