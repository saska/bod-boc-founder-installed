import QtQuick 2.0
import "../../Components/TextBoxCanvas"
import "../../Components/Circle"
import "../../Components/Utils/Constants.js" as Constants

Item {
    id: box

    property alias listModel : boxListView.model
    property string locale : currentLocale
    property var title : currentTitle
    property double cacheBuffer: (listModel.count * 1920 * Constants.APP_SCALE)
    property string currentYear : listModel.count > 0 ? listModel.get(boxListView.currentIndex).month[box.locale].toUpperCase() + " " + listModel.get(boxListView.currentIndex).year[box.locale] : ""

    property bool fromBoxListView : false

    property int count : 0
    property int magicNumber: 15

    opacity: 0.0 // hide while loading the data

    ListView{
        id: boxListView
        anchors.fill: parent
        cacheBuffer: box.cacheBuffer
        interactive: nav.visible

        pressDelay: 2000

        delegate: Item{
            id: boxListItem
            width: ListView.view.width
            height: ListView.view.height

            Loader{
                id: boxItemLoader
                anchors.fill: parent
                source: "BoxPageDelegate.qml"
                property var currentYear : model.year
                property string currentFolderName: model.folderName
                property ListModel currentPeople : ListModel{}
                property var currentMainPerson
                property string currentLocale : box.locale
                property int currentCount: 0
                property int currentPeopleCount: 0
                property bool currentGroupPicture : model.groupPicture
                asynchronous: true

                onLoaded: {
                    boxItemLoader.initDelegate()
                    box.count++
                    if(box.count == box.magicNumber){
                      delay.start()
                    }
                }

                function initDelegate(){
                    var people = model[box.locale]
                    var data = people.data

                    boxItemLoader.currentCount = 0
                    boxItemLoader.currentPeople.clear()

                    for(var i = 0; i < data.length; i++){
                        if(i > 0)
                            boxItemLoader.currentPeople.append(data[i])
                        else
                            boxItemLoader.currentMainPerson = data[i]

                        boxItemLoader.currentCount++
                    }
                    boxItemLoader.currentPeopleCount = data.length - 1
                }
            }
        }

        onCurrentIndexChanged: {
            fromBoxListView = true
            boxMenu.selectedIndex = currentIndex
            //debugItem.setText('current index: ' + currentIndex)
        }

        // TODO: Tune following params:
        flickDeceleration: 200 //20000
        highlightMoveDuration: 200

        //preferredHighlightBegin: 0
        //preferredHighlightEnd: 0
        //highlightRangeMode: ListView.StrictlyEnforceRange
        //highlightFollowsCurrentItem: true
        snapMode: ListView.SnapOneItem
        orientation: ListView.Horizontal

        property int fakeCurrentIndex : listModel.count - n
        property int n : 1

        onMovementEnded: {
            var pageWidth = contentWidth/listModel.count
            n = -1/pageWidth * (contentX - contentWidth)
            currentIndex = fakeCurrentIndex
        }

        opacity: boxMenu.state == "open" ? 0.5 : 1.0

        Behavior on opacity{
            NumberAnimation{ duration: 1000; easing.type: Easing.InOutSine}
        }
    }

    Item{
        anchors.fill: boxListView
        visible: boxMenu.state == "open"
        MouseArea{
            anchors.fill: parent
            onClicked: {
                boxMenu.state = "closed"
            }
        }
    }

    BoxMenu{
        id: boxMenu
        listModel: box.listModel
        locale: box.locale
        onHeightChanged: {
            localeSwitch.bottomOffset = boxMenu.height
        }
        onClosedWithIndex: {
            debugItem.setText('test: ' + idx)
            boxListView.positionViewAtIndex(boxMenu.selectedIndex, ListView.Beginning)
            boxListView.currentIndex = boxMenu.selectedIndex
        }
    }

    Timer{
        id: delaySelectionFromMenu
        interval: 1000
        onTriggered: {
            //boxMenu.handleClicked()
            boxListView.positionViewAtIndex(boxMenu.selectedIndex, ListView.Beginning)
            boxListView.currentIndex = boxMenu.selectedIndex

        }
    }

    // Text
    TextBoxCanvas{
        id: textBox
        anchors.centerIn: parent
        anchors.horizontalCenterOffset: textBox.isLeft ? 215 : -215
        visible: false
        //transform: Rotation { origin.x: textBox.height/2; origin.y: textBox.width/2; axis { x: 0; y: 1; z: 0 } angle: 180 }

    }

    Item{
        id: pageDelegateSpareItem
        property int prevX : 0
        property int prevY : 0
        property alias source: pageDelegateSpareItemImage.source
        property double circleScale : 0.5
        property alias nameTag : pageDelegateSpareItemImage.text
        property alias stripsVisible : pageDelegateSpareItemImage.stripsVisible

        visible: false

        Item{
            width: parent.width - 10
            height: parent.height - 10
            anchors.centerIn: parent

            CircleFramed{
                id: pageDelegateSpareItemImage
                stripsVisible: false
                anchors.centerIn: parent
                text: "name"
                scale: pageDelegateSpareItem.circleScale
            }
        }
        z: 10
    }

    Item{
        id: nav
        anchors.fill: parent

        Image{
            id: navLeft
            source: "../../Assets/Images/image-nav-left.png"
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 50
            visible: boxListView.currentIndex > 0
            opacity: boxListView.moving ? 0.0 : 1.0

            Behavior on opacity{
                SmoothedAnimation{}
            }

            MouseArea{
                anchors.fill: parent
                enabled: parent.opacity > 0.9
                onClicked: {
                    //boxListView.decrementCurrentIndex()
                    //boxListView.positionViewAtIndex(boxListView.currentIndex - 1, ListView.Beginning)
                    //boxListView.currentIndex -= 1
                    var pageWidth = boxListView.contentWidth/listModel.count
                    boxListView.contentX -= pageWidth
                    boxListView.currentIndex -= 1

                }

            }
        }

        Image{
            id: navRight
            source: "../../Assets/Images/image-nav-right.png"
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 50
            visible: boxListView.currentIndex < boxListView.count - 1
            opacity: boxListView.moving ? 0.0 : 1.0


            Behavior on opacity{
                SmoothedAnimation{}
            }

            MouseArea{
                anchors.fill: parent
                enabled: parent.opacity > 0.9
                onClicked: {
                    var pageWidth = boxListView.contentWidth/listModel.count
                    boxListView.contentX += pageWidth
                    boxListView.currentIndex += 1
                }
            }
        }
    }

    Timer{
        id: delay
        interval: 1000
        onTriggered: {
            boxListView.positionViewAtIndex(14, ListView.End)
            nextDelay.start()
        }
    }

    Timer{
        id: nextDelay
        interval: 2000
        onTriggered: {
            boxListView.currentIndex = box.listModel.count - 1
            loadingItem.hide();
        }
    }

    Connections{
        target: loadingItem
        onHidden: {
            box.opacity = 1.0
            localeSwitch.setRight()
        }
    }

    Behavior on opacity{
        NumberAnimation{ duration: 500; easing.type: Easing.OutSine }
    }

    Component.onCompleted: {

    }

    QtObject{
        id: simulateObject

        property int currentPeopleCount : 0
        property int currentSelectedPersonIndex : -1
        property int timesInPage : 0

        property int offsetStart : 5000

        signal simulateSelectPersonInGrid(int selectedPersonIndex)
        signal peopleCount(int count)
        signal getPeopleCount()

        onPeopleCount: {
            console.log('people count: ' + count)
            var personIndex = Math.floor(Math.random()*count) - 1
            if(personIndex < -1 || personIndex > count - 1){
                personIndex = -1
            }

            simulateObject.simulateSelectPersonInGrid(personIndex)
            //debugItem.setText('picked: ' + personIndex)
        }
    }

    function nextPage(){

        //debugItem.setText('times in page: ' + simulateObject.timesInPage)
        console.log('next page')

        if(simulateObject.timesInPage++ > 1){
            if(boxListView.currentIndex == 0){
                forward = true
            }else if(boxListView.currentIndex == listModel.count - 1){
                forward = false
            }

            if(forward)
                boxListView.incrementCurrentIndex()
            else
                boxListView.decrementCurrentIndex()

            simulateObject.timesInPage = 0
        }
    }

    Connections{
        target: idleObject
        onWakeup: {
            if(idleTimeline.running) {
                idleTimeline.stop()
            }
            idleRect.visible = false
        }
        onSleep: {
            if(idleState == "IDLE"){
                if(!idleTimeline.running){
                    checkBeforeIdle()
                    idleRect.visible = false
                    idleTimeline.start()
                }
            }else if(idleState == "IDLE_LONG"){
                idleTimeline.stop()
                idleRect.visible = true
            }
        }
    }

    function checkBeforeIdle(){
        // textBox
        simulateObject.offsetStart = 5000

        if(textBox.isOpen) {
            simulateObject.offsetStart += 5000
            textBox.simulateClose()
        }

        if(boxMenu.isOpen) {
            simulateObject.offsetStart += 5000
            boxMenu.handleClicked()
        }

        console.log('boxMenu.isOpen: ' + boxMenu.isOpen)
    }

    property int tick: 0
    property bool forward : false

    SequentialAnimation on tick{
        id: idleTimeline
        running:  false //!loadingItem.loading
        loops: Animation.Infinite
        alwaysRunToEnd: false

        ScriptAction{ script:  debugItem.setText('idle animation started with offset start: ' + simulateObject.offsetStart)}
        PauseAnimation { duration: simulateObject.offsetStart }
        ScriptAction{ script:  simulateObject.getPeopleCount()}
        //ScriptAction{ script:  debugItem.setText('get people executed')}
        PauseAnimation { duration: 8000 }

        SequentialAnimation{
            loops: 3
            ScriptAction{ script: textBox.simulateFlick()}
            PauseAnimation { duration: 1000 }
        }

        PauseAnimation { duration: 500 }
        ScriptAction{ script: localeSwitch.simulateSwitchLocale()}
        PauseAnimation { duration: 500 }

        SequentialAnimation{
            loops: 3
            ScriptAction{ script: textBox.simulateFlick()}
            PauseAnimation { duration: 1000 }
        }

        ScriptAction{ script: textBox.simulateClose()}
        PauseAnimation { duration: 8000 }
        ScriptAction{ script: localeSwitch.simulateSwitchLocale()}
        PauseAnimation { duration: 3000 }
        ScriptAction{ script: localeSwitch.simulateSwitchLocale()}
        PauseAnimation { duration: 3000 }
        ScriptAction {script: boxMenu.handleClicked()}
        PauseAnimation { duration: 5000 }
        ScriptAction {script: boxMenu.handleClicked()}
        PauseAnimation { duration: 3000 }
        ScriptAction{ script: box.nextPage()}
        PauseAnimation { duration: 3000 }
    }
}
