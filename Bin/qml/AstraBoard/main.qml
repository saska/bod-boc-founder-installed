import QtQuick 2.0
import AstraMuseum 1.0

import "Components/Utils"
import "Components/Circle"
import "Components/TitleText"
import "Components/Locale"
import "Components/Utils/Sprintf.js" as Helper
import "Components/Utils/Constants.js" as Constants


Item{
    id: container

    signal listDone()
    signal readDone()
    signal readTextDone()

    property bool debugMode : false

    /*Video{
        id: video
       anchors.fill: parent
    }*/

    FontLoader{
        source: "Assets/Fonts/frutigerltstdroman.ttf"
    }

    FontLoader{
        source: "Assets/Fonts/frutigerltstdbold.ttf"
    }

    FontLoader{
        source: "Assets/Fonts/frutigerltstdultrablack.ttf"
    }

    Loader{
        id: loader

        width: 1920
        height: 1080
        scale: Constants.APP_SCALE
        anchors.centerIn: parent

        source: "Settings.qml"

        property ListModel currentListModel: ListModel{}
        property var currentTitle : {"en_US" : "", "id_ID": "" }
        property var currentMenuTitle : {"en_US" : "", "id_ID": "" }
        property string currentLocale: currentBoard.locale
    }

    QtObject{
        id: currentBoard
        property var title : {"en_US" : "", "id_ID": "" }
        property var menuTitle : {"en_US" : "", "id_ID": "" }
        property ListModel pages: ListModel{}
        property int pageReadCount : 0
        property string locale: "id_ID"
    }

    WorkerScript{
        id: pagesScript
        source: "Pages.js"
        onMessage: {
            var folderNames = messageObject.folderNames
            for(var i = 0; i < folderNames.length; i++ ){
                var folderName = folderNames[i]
                Content.read(folderName, i, "en_US")
                Content.read(folderName, i, "id_ID")
            }

            currentBoard.title = messageObject.title
            currentBoard.menuTitle = messageObject.menuTitle
        }
    }

    Connections{
        target: Content
        onListResponse: {
            currentBoard.pageReadCount = 0
            pagesScript.sendMessage({pages: currentBoard.pages, jsonData : jsonData})
        }

        onReadResponse: {
            //var data = JSON.parse(serializedData)
            var data = jsonData
            var page = currentBoard.pages.get(index)
            var people = { data : [], year: page.year[lang], folderName: page.folderName }
            var positions = data.positions
            var groupPicture = data.groupPicture ? data.groupPicture : false

            var count = 0
            var isLeft = true

            for(var i = 0; i < positions.length; i++){
                var images = data[i]
                var path = Helper.sprintf("file:///%s/Contents/%s/People/%s/%s", Content.rootPath, Content.dataType, folderName, i )

                var peeps = []

                for(var j = 0; j < images.length; j++){
                    var name = images[j].split(".jpg")[0];
                    if(name.indexOf('_') > -1) name = name.split("_")[1];
                    var person = {
                        nameTag: handle(name, jsonData.shortNames),
                        name : name,
                        image : Helper.sprintf("%s/%s", path, images[j]),
                        // position : positions[i],
                        visual: { opacity: 1.0, scale: 1.0},
                        isLeft : isLeft,
                        selected : false
                    }
                    //people.data.push(person)
                    peeps.push(person)

                    count++
                    if(count > 4) count = 1
                    isLeft = (count < 3)
                }

                // peeps.reverse()

                for(var k = 0; k < peeps.length; k++){
                    people.data.push(peeps[k])
                }
            }

            currentBoard.pages.get(index).groupPicture = groupPicture
            currentBoard.pages.get(index)[ lang ] = people
            currentBoard.pageReadCount++

            if(currentBoard.pageReadCount ==  2 * currentBoard.pages.count){
                loader.currentTitle = currentBoard.title
                for(var l = 0; l < currentBoard.pages.count; l++){
                  loader.currentListModel.append(currentBoard.pages.get(l))
                }

                currentBoard.pages.clear()
                container.readDone()
            }
        }
    }

    Component.onCompleted: {
        videoCall.start()
    }

    Timer{
        id: videoCall
        repeat: false
        interval: 2000
        onTriggered: {
            BackgroundVideo.call("C:\\Program Files\\GRETECH\\GomPlayer\\GOM","C:\\App\\Data\\Contents\\Video\\background.mp4")
        }
    }

    LocaleSwitch{
        id: localeSwitch
        scale: 1280/1920
        isID: true

        property int horizontalCenterOffset: 0
        property int bottomOffset: 0

        function setCenter(){
            horizontalCenterOffset = 0
            bottomOffset = Constants.APP_SCALE * 50
        }

        function setRight(){
            localeSwitch.horizontalCenterOffset = Constants.APP_SCALE * ((1920/2) - 280)//Constants.APP_SCALE * (1920 - 200)
            localeSwitch.bottomOffset = 0
        }

        onIsIDChanged: {
            currentBoard.locale = localeSwitch.isID ? "id_ID" : "en_US"
        }
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom

        anchors.bottomMargin: localeSwitch.bottomOffset
        anchors.horizontalCenterOffset: localeSwitch.horizontalCenterOffset

    }

    FpsItem{
        id: fpsItem
        onClicked: {
            debugItem.visible = debugItem.visible ? false : true
        }
        visible: container.debugMode
    }

    DebugItem{ id: debugItem; visible: container.debugMode}
    LoadingItem{
        id: loadingItem
        onLoadingChanged: {
            if(loading) {
                localeSwitch.isID = false
                localeSwitch.visible = false
                idleObject.stop()
            }
            else{
                localeSwitch.isID = true
                localeSwitch.visible = true
                idleObject.start()
                console.log('to: ' + idleObject.getTimeout())
                idleObject.setMouseVisible(container.debugMode)
            }
        }
    }

    Item{
        id: idleRect
        anchors.fill: parent
        visible: false
        Rectangle{
            anchors.fill: parent
            opacity: 0.8
            color: "black"
            MouseArea{
                anchors.fill: parent
            }
        }

        Text{
            anchors.centerIn: parent
            text: "Tap here to discover."
            color: "white"
            font.pixelSize: Constants.APP_SCALE * 40
            font.family: Constants.FONT_LIGHT_FAMILY
        }
    }

    Image{
        id: leftLogo
        source: Constants.LOGO_LEFT
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.leftMargin: 120
    }

    Image{
        id: rightLogo
        source: Constants.LOGO_RIGHT
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.rightMargin: 120
    }

    function handle(text, shortNames){
        var textArr = text.split(" ")
        var textMod = ''
        if(textArr.length > 3){
            for(var i = 0; i < textArr.length; i++){
                if(i == 0 || i == textArr.length - 1)
                    textMod += textArr[i] + ' '
                else
                    textMod += textArr[i].substr(0,1) + ". "
            }

            if(shortNames){
                if(shortNames[text]){
                    textMod = shortNames[text]
                }
            }

            return textMod.trim();

        }else{
            return text;
        }
    }
}
