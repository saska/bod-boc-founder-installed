import QtQuick 2.0
import "Components/Utils/Constants.js" as Constants

Item {
    id: settings

    Item{
        id: listMenu
        width: 600
        height: 200
        anchors.centerIn: parent

        ListModel{
            id: settingsModel
            ListElement{
                text : "Founder"
                url : "Pages/Founder/FounderBox.qml"
            }

            ListElement{
                text : "BOD"
                url : "Pages/BOX/BodBox.qml"
            }

            ListElement{
                text : "BOC"
                url : "Pages/BOX/BocBox.qml"
            }
        }

        ListView{
            anchors.fill: parent
            interactive: false
            model: settingsModel
            orientation: ListView.Horizontal
            delegate: Item{

                width: 200
                height: 200

                Rectangle{
                    anchors.centerIn: parent

                    width: 180
                    height: 180

                    radius: width/2
                    antialiasing: true

                    Text {
                        text: model.text
                        anchors.centerIn: parent
                        font.pixelSize: 30
                        font.family: Constants.FONT_ROMAN_FAMILY
                        color: Constants.THEME_COLOR
                    }


                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            loader.source = Qt.resolvedUrl(model.url)
                        }
                    }
                }
            }
        }
    }

    Rectangle{
        id: bar
        width: 600
        height: 40
        color: "white"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: listMenu.bottom
        anchors.topMargin: 20

        Rectangle{
            id: line
            width: rect.x
            height: 40
            color: Constants.THEME_COLOR
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                rect.x = mouseX
            }
        }

        Item{
            id: rect
            width: 600
            height: 40
            x: 300

            opacity: 0.5
            //color: "red"

            MouseArea{
                anchors.fill: parent
                drag.target: rect
                drag.minimumX: 0
                drag.maximumX: parent.width
                drag.axis: Drag.XAxis

                onClicked: {
                    if(mouseX < bar.width - rect.x)
                    rect.x = rect.x + mouseX
                    debugItem.setText('rect x: ' + rect.x + ' ' + mouseX);
                }
            }

            onXChanged: {
                setTimeout()
            }
        }
    }

    Text{
        id: idle
        text: (rect.x/600 * 10).toFixed(2) + " minutes"
        anchors.left:bar.right
        anchors.leftMargin: 10
        anchors.verticalCenter: bar.verticalCenter
        font.pixelSize: 30
        font.family: Constants.FONT_ROMAN_FAMILY
        color: "white"
    }

    Rectangle{
        id: d
        width: 50
        height: 50

        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 30

        color: container.debugMode ? Constants.THEME_COLOR : "white"

        opacity: dm.pressed ? 0.5 : 1.0

        Text{
            anchors.centerIn: parent
            font.pixelSize: 12
            font.family: Constants.FONT_ROMAN_FAMILY
            text: container.debugMode ? "debug" : "release"
            color: container.debugMode ? "white" : Constants.THEME_COLOR
        }

        MouseArea{
            id: dm
            anchors.fill: parent
            onClicked: {
                container.debugMode = container.debugMode ? false : true
            }
        }
    }


    function setTimeout(){
        var minutes = (rect.x/600 * 10).toFixed(3)
        var msecs = minutes * 60 * 1000
        idleObject.setTimeout(msecs)
    }

    Component.onCompleted: {
        debugItem.setText("**DEBUG**");
        localeSwitch.setCenter()
        setTimeout()
        idleObject.stop()
        //console.log('tos: ' + idleObject.getTimeout())
    }
}
