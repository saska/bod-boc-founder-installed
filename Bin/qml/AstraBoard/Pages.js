WorkerScript.onMessage = function(message) {
    var jsonData = message.jsonData
    message.pages.clear()

    var folderNames = jsonData.folderNames
    var enUSData = jsonData["en_US"]
    var idIDData = jsonData["id_ID"]

    var title = {
        "en_US" : enUSData.title.toUpperCase(),
        "id_ID" : idIDData.title.toUpperCase()
    }

    var menuTitle = {
        "en_US" : enUSData.menuTitle,
        "id_ID" : idIDData.menuTitle
    }

    for(var i = 0; i < folderNames.length; i++){
        var obj = {}
        obj.folderName = folderNames[i]

         var arr_en_US = enUSData.years[i].split(" ")
        var arr_id_ID = idIDData.years[i].split(" ")

        obj.year = {
            "en_US" : arr_en_US.length > 1 ? arr_en_US[1] : enUSData.years[i],
            "id_ID" : arr_id_ID.length > 1 ? arr_id_ID[1] : idIDData.years[i],
        }

        obj.month = {
            "en_US" : arr_en_US.length > 1 ? arr_en_US[0] : "",
            "id_ID" : arr_id_ID.length > 1 ? arr_id_ID[0] : "",
        }

        obj.hasMonth = arr_en_US.length > 1

        obj.duration = 0
        obj.offset = 300

        obj.en_US = {}
        obj.id_ID = {}

        message.pages.append(obj)
    }

    if(message.pages.count > 12){
        var duration = 500
        for(var j = message.pages.count - 1; j >= message.pages.count - 12; j-- ){
            message.pages.get(j).duration = duration
            duration += 100
        }
    }

    message.pages.sync()
    WorkerScript.sendMessage({folderNames : folderNames, title: title, menuTitle : menuTitle})
}
