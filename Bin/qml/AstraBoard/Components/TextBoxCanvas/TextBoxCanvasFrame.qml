/****************************************************************************
**
** Copyright (C) 2013 Labtek
** All rights reserved.
** Author: Dhi Aurrahman <dio@labtekindie.com>
**
** This file is part of the Museum Astra Project
**
****************************************************************************/

import QtQuick 2.0
import QtGraphicalEffects 1.0
import "../../Components/Utils/Constants.js" as Constants

Item {
    id: root
    width: background.width + 10
    height: background.height + 10
    antialiasing: true

    Image {
        id: background
        source: "../../Assets/Images/image-textbox-background.png"
        visible: false
        anchors.centerIn: parent
    }

    property alias canvasContainerState: canvasContainer.state

    Item{
        id: canvasContainer

        property int offset: 8
        width: background.width + offset
        height: background.height + offset

        property int duration : 300

        antialiasing: true

        anchors.centerIn: parent

        state: "nothing"

        states: [
            State {
                name: "nothing"
            },
            State {
                name: "something"
            }
        ]
        transitions: [
            Transition {
                from: "nothing"
                to: "something"
                SequentialAnimation{
                    PropertyAction { target: canvas; property: "opacity"; value: 1.0}
                    PropertyAction { target: canvas; property: "direction"; value: "down";}
                    NumberAnimation { target: canvas; property: "delta"; from: 0; to: canvas.rectHeight; duration: canvasContainer.duration}
                    PropertyAction { target: canvas; property: "direction"; value: "right"}
                    NumberAnimation { target: canvas; property: "delta"; from: 0; to: canvas.rectWidth - canvas.radius}
                    PropertyAction { target: canvas; property: "direction"; value: "arc"}
                    NumberAnimation { target: canvas; property: "delta"; from: Math.PI/2; to: 0; duration: canvasContainer.duration/10}
                    PropertyAction { target: canvas; property: "direction"; value: "up"}
                    NumberAnimation { target: canvas; property: "delta"; from: canvas.rectHeight - canvas.radius; to: canvas.recty; duration: canvasContainer.duration}
                    PropertyAction { target: canvas; property: "direction"; value: "left"}
                    NumberAnimation { target: canvas; property: "delta"; from: canvas.rectWidth; to: canvas.rectx; duration: canvasContainer.duration; easing.type: Easing.OutSine}
                }
            },

            Transition {
                from: "something"
                to: "nothing"
                SequentialAnimation{
                    /*PropertyAction { target: canvas; property: "direction"; value: "left"}
                    NumberAnimation { target: canvas; property: "delta"; from: canvas.rectx; to: canvas.rectWidth + canvas.rectx; duration: canvasContainer.duration}
                    PropertyAction { target: canvas; property: "direction"; value: "up"}
                    NumberAnimation { target: canvas; property: "delta"; from: 0; to: canvas.rectHeight - canvas.radius - canvas.recty; duration: canvasContainer.duration}
                    PropertyAction { target: canvas; property: "direction"; value: "arc"}
                    NumberAnimation { target: canvas; property: "delta"; from: 0; to: Math.PI/2; duration: canvasContainer.duration/10}
                    PropertyAction { target: canvas; property: "direction"; value: "right"}
                    NumberAnimation { target: canvas; property: "delta"; from: canvas.rectWidth - canvas.radius; to: 0; duration: canvasContainer.duration}
                    PropertyAction { target: canvas; property: "direction"; value: "down"}
                    NumberAnimation { target: canvas; property: "delta"; from: canvas.rectHeight; to: 0; duration: canvasContainer.duration; easing.type: Easing.OutSine}*/

                    NumberAnimation { target: canvas; property: "opacity"; to: 0; duration: 250}
                }
            },
            Transition {
                from: "something"
                to: "switchlocale"
                SequentialAnimation{

                }

            }
        ]

        Canvas {
            id:canvas
            anchors.fill: parent

            antialiasing: true
            property int radius: 50
            property int rectx: canvasContainer.offset/2
            property int recty: canvasContainer.offset/2
            property int rectWidth: width - canvasContainer.offset
            property int rectHeight: height - canvasContainer.offset

            property double delta: 0

            Behavior on delta {
                NumberAnimation { duration: 500 }
            }

            property string direction: 'down'

            onDeltaChanged: {
                requestPaint()
            }

            onPaint: {
                var ctx = getContext("2d");
                ctx.save();

                ctx.clearRect(0,0, canvas.width, canvas.height);
                ctx.strokeStyle = "white"
                ctx.lineWidth = canvasContainer.offset/2
                ctx.fillStyle = false
                ctx.globalAlpha = 1.0

                ctx.beginPath();
                ctx.moveTo(rectx, recty);

                switch(direction){
                    case 'left':
                        ctx.lineTo(rectx, recty + rectHeight);
                        ctx.lineTo(rectx, recty + rectHeight);
                        ctx.lineTo(rectx + rectWidth - radius, recty + rectHeight);
                        ctx.arc(rectx + rectWidth - radius, recty + rectHeight - radius, radius, Math.PI/2, 0, true)
                        ctx.lineTo(rectx + rectWidth, recty + recty/2);
                        ctx.lineTo(delta, recty + recty/2);
                    break;
                    case 'up':
                        ctx.lineTo(rectx, recty + rectHeight);
                        ctx.lineTo(rectx, recty + rectHeight);
                        ctx.lineTo(rectx + rectWidth - radius, recty + rectHeight);
                        ctx.arc(rectx + rectWidth - radius, recty + rectHeight - radius, radius, Math.PI/2, 0, true)
                        ctx.lineTo(rectx + rectWidth, delta);

                    break;
                    case 'arc':
                        ctx.lineTo(rectx, recty + rectHeight);
                        ctx.lineTo(rectx, recty + rectHeight);
                        ctx.lineTo(rectx + rectWidth - radius, recty + rectHeight);
                        ctx.arc(rectx + rectWidth - radius, recty + rectHeight - radius, radius, Math.PI/2, delta, true)
                    break;
                    case 'right' :
                        ctx.lineTo(rectx, recty + rectHeight);
                        ctx.lineTo(rectx, recty + rectHeight);
                        ctx.lineTo(rectx + delta, recty + rectHeight);
                    break;
                    case 'down' :
                        ctx.lineTo(rectx, recty + delta);
                    break;
                    default: break;
                }

                //ctx.closePath();
                ctx.stroke();
                ctx.restore();
            }
        }
    }
}
