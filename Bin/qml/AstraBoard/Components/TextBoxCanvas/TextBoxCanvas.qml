import QtQuick 2.0
import QtGraphicalEffects 1.0
import "../../Components/Utils/Constants.js" as Constants

Item {
    id: root

    property alias titleText: contentContainer.title
    property alias subtitleText: contentContainer.subtitle
    property alias contentText: contentContainer.text

    property alias contentState: contentContainer.state


    width: background.width + 100
    height: background.height

    antialiasing: true

    property int selectedIndex: -1
    property bool isLeft: true
    property bool busy: false
    property bool contentBusy: contentContainer.busy
    property bool isOpen: false

    signal closing();
    signal closed();
    signal finished();
    signal loadLocale()

    signal flickingLimit()

    function swicthLocale(){
        contentContainer.state = Constants.STATE_SWITCHLOCALE
    }

    function simulateFlick(){
        contentContainer.simulateFlick()
    }

    function simulateClose(){
        if(!busy && isOpen){
            root.state = "nothing"
            root.closing()
        }
    }

    onVisibleChanged: {
        if(visible){
            timer.start()
        }
    }

    Timer{
        id: timer
        interval: 50
        onTriggered: {
            contentContainer.backToTop()
            root.state = "something"

        }
    }

    Item{
        id: lineItem
        property int offset: 0
        width: offset
        height: 4
        anchors.verticalCenter: textBoxHelper.verticalCenter
    }

    Rectangle{
        id: lineRight
        anchors.right: parent.right
        width: lineItem.width
        height: lineItem.height
        anchors.verticalCenter: textBoxHelper.verticalCenter
        visible: !isLeft

    }

    Rectangle{
        id: lineLeft
        anchors.left: parent.left
        width: lineItem.width
        height: lineItem.height
        anchors.verticalCenter: textBoxHelper.verticalCenter
        visible: isLeft
    }

    Item{
        id: textBoxHelper
        anchors.fill: root

        transform: Rotation { origin.x: textBoxHelper.width/2; origin.y: textBoxHelper.height/2; axis { x: 0; y: 1; z: 0 } angle: root.isLeft ? 0 : 180 }

        Image{
            id: handle
            property int offset: 0

            source: "../../Assets/Images/image-textbox-handle.png"
            antialiasing: true
            opacity: 0.0
            anchors.verticalCenter: background.verticalCenter
            anchors.verticalCenterOffset: offset
            anchors.right: background.left
            anchors.rightMargin: -4
        }

        TextBoxCanvasBackground{
            id: veil
            anchors.centerIn: parent
        }

        TextBoxCanvasFrame {
            id: background
            anchors.centerIn: parent
            visible: true
        }

        Item{
            id: closeButtonItem

            anchors.top: background.top
            anchors.right: background.right
            anchors.rightMargin: 4
            anchors.topMargin: 6

            width: closeButton.width + 40
            height: closeButton.height + 40

            Image{
                id: closeButton
                opacity: 0.0
                source: "../../Assets/Images/image-textbox-close.png"
                anchors.top: parent.top
                anchors.right: parent.right
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(!busy){
                        root.state = "nothing"
                        root.closing()
                    }
                }
            }
        }
    }

    TextBoxCanvasContent{
        id: contentContainer
        anchors.centerIn: parent
        onLoadLocale:{
            root.loadLocale()
        }
        onFlickingLimit: {
            root.flickingLimit()
        }
    }


    onFinished: {
        if(state == "nothing"){
            root.closed();
            isOpen = false
        }
        else{
            isOpen = true
        }
    }

    state: "nothing"

    states: [
        State{
            name : "something"
            //PropertyChanges { target: root; opacity : 1.0}
        },
        State{
            name : "nothing"
            //PropertyChanges { target: root; opacity : 0.0}
        }
    ]

    transitions: [
        Transition{
            from : "nothing"; to: "something";

            SequentialAnimation{
                PropertyAction { target: root; property: 'busy'; value: true}
                NumberAnimation{ target: lineItem; property: "offset"; to: handle.x + handle.width; duration: 200}

                NumberAnimation{ target: handle; property: "opacity"; to: 1.0; duration: 100}
                NumberAnimation{ target: handle; property: "offset"; to: - (background.height/2 - handle.height/2 - 4); duration: 200}
                PropertyAction { target: background; property: 'canvasContainerState'; value: "something"}
                PauseAnimation { duration: 1200 }
                PropertyAction { target: veil; property: 'state'; value: "something"}
                PauseAnimation { duration: 500 }
                PropertyAction { target: contentContainer; property: 'state'; value: "something"}
                NumberAnimation { target: closeButton; property: 'opacity'; to: 1.0}


                PropertyAction { target: root; property: 'busy'; value: false}
                ScriptAction { script: root.finished()}
            }

        },
        Transition{
            from : "something"; to: "nothing";

            SequentialAnimation{
                PropertyAction { target: root; property: 'busy'; value: true}
                NumberAnimation { target: closeButton; property: 'opacity'; to: 0.0}
                //PropertyAction { target: contentContainer; property: 'state'; value: "nothing"}
                //PauseAnimation { duration: 200 }
                //PropertyAction { target: veil; property: 'state'; value: "nothing"}
                //PropertyAction { target: background; property: 'canvasContainerState'; value: "nothing"}
                //PauseAnimation { duration: 250 }

                PropertyAction { target: contentContainer; property: 'state'; value: "nothing"}
                PropertyAction { target: veil; property: 'state'; value: "nothing"}
                PropertyAction { target: background; property: 'canvasContainerState'; value: "nothing"}

                PauseAnimation { duration: 250 }

                ParallelAnimation{
                    NumberAnimation{ target: handle; property: "offset"; to: 0; duration: 300; easing.type: Easing.Linear }
                    NumberAnimation{ target: handle; property: "opacity"; to: 0.0; duration: 300; easing.type: Easing.InOutSine }
                }

                ParallelAnimation{
                    NumberAnimation{ target: lineItem; property: "offset"; to: 0; duration: 300; easing.type: Easing.InOutSine }
                }



                //NumberAnimation{ target: lineItem; property: "offset"; to: 0; duration: 200}

                //NumberAnimation{ target: lineItem; property: "offset"; to: handle.x + handle.width; duration: 500}
                PropertyAction { target: root; property: 'busy'; value: false}
                ScriptAction { script: root.finished()}

            }
        }

    ]


    /*TextBoxCanvasHandle{
        id: handle
        anchors.centerIn: parent
    }

    TextBoxCanvasFrame {
        id: background
        //source: "../../Assets/Images/image-textbox-background.png"
        visible: true
    }

    TextBoxCanvasBackground{
        id: movingBackground
        anchors.centerIn: parent
    }

    Text{
        text: "is left side: " + isLeft
        anchors.centerIn: parent
        font.pixelSize: 30
    }

    Text{
        id: title
        anchors.centerIn: parent
    }
    Text{
        id: subtitle
        anchors.top: title.bottom
    }
    Text{
        id: content
        anchors.top: subtitle.bottom
    }

    Rectangle{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 100
        height: 100

        MouseArea{
            anchors.fill: parent
            onClicked: {
                //root.closing()
                handle.state = "something"
            }
        }

    }

    z: 100

    Component.onCompleted: {

    }*/

}
