/****************************************************************************
**
** Copyright (C) 2013 Labtek
** All rights reserved.
** Author: Dhi Aurrahman <dio@labtekindie.com>
**
** This file is part of the Museum Astra Project
**
****************************************************************************/

import QtQuick 2.0
import QtGraphicalEffects 1.0
import "../../Components/Utils/Constants.js" as Constants

Item{
    id: root
    width : Constants.TEXTBOX_HANDLE_WIDTH * Constants.GLOBAL_SCALE
    height : Constants.TEXTBOX_HANDLE_HEIGHT * Constants.GLOBAL_SCALE

    property double angle: 0.0
    property bool busy: false
    signal finished()
    signal originalPos();

    property int offsetSetting : -100
    property int offset : 0

    anchors.verticalCenter: parent.verticalCenter
    anchors.verticalCenterOffset: offset

    property string assetsPrefix : "../../"
    property string source: assetsPrefix + "Assets/Images/image-textbox-handle.png"

    opacity: 0.0

    Item{
        id: box
        width : frame.width
        height : frame.height

        Image{
            id: frame
            source: root.source
            antialiasing: true

            sourceSize.width : Constants.TEXTBOX_HANDLE_WIDTH * Constants.GLOBAL_SCALE
            sourceSize.height : Constants.TEXTBOX_HANDLE_HEIGHT * Constants.GLOBAL_SCALE
        }
    }

    state: Constants.STATE_NOTHING

    states: [
        State{
            name : Constants.STATE_SOMETHING
            //PropertyChanges { target: root; opacity : 1.0}
        },
        State{
            name : Constants.STATE_NOTHING
            //PropertyChanges { target: root; opacity : 0.0}
        }
    ]

    transitions: [
        Transition{
            from : Constants.STATE_NOTHING; to: Constants.STATE_SOMETHING;

            SequentialAnimation{
                PropertyAction { target: root; property: 'busy'; value: true}
                //BlinkingAnimation{ target: root}
                NumberAnimation {target: root; property: 'opacity'; to: 1.0}
                PauseAnimation { duration: 800 }
                NumberAnimation { target: root; property: "offset"; from: 0; to: root.offsetSetting ; duration: Constants.ANIMATION_DURATION_SLOW * 2; easing.type: Easing.OutSine }
                //BlinkingAnimation{ target: root}
                PropertyAction { target: root; property: 'busy'; value: false}
                ScriptAction { script: root.finished()}
            }

        },
        Transition{
            from : Constants.STATE_SOMETHING; to: Constants.STATE_NOTHING;

            SequentialAnimation{
                PropertyAction { target: root; property: 'busy'; value: true}
                //BlinkingAnimation{ target: root}
                ParallelAnimation{
                    NumberAnimation { target: root; property: "offset"; from: root.offsetSetting; to: 0; duration: Constants.ANIMATION_DURATION_SLOW }
                    SequentialAnimation{
                        PauseAnimation { duration: 500 }
                        ScriptAction { script: root.originalPos()}
                    }
                }
                NumberAnimation {target: root; property: 'opacity'; to: 0.0; duration: 500}
                PropertyAction { target: root; property: 'busy'; value: false}
                ScriptAction { script: root.finished()}

            }
        }

    ]
}
