/****************************************************************************
**
** Copyright (C) 2013 Labtek
** All rights reserved.
** Author: Dhi Aurrahman <dio@labtekindie.com>
**
** This file is part of the Museum Astra Project
**
****************************************************************************/

import QtQuick 2.0
import QtGraphicalEffects 1.0
import "../Effects"
import "../../Components/Utils/Constants.js" as Constants

Item {
    id: root

    width : box.width
    height : box.height

    property double angle: 0.0
    property string assetsPrefix : "../../"
    property string source: assetsPrefix + "Assets/Images/image-textbox-background.png"

    property bool busy: false

    property int startOffset : 8
    property int endOffset : 220 * Constants.GLOBAL_SCALE
    property int textAppearOffset : 30
    property int margins : 0
    property string title : "Test Test"
    property string subtitle : "Test Test"
    property string text : "Test Test"
    property int offset : startOffset

    signal finished()
    signal loadLocale()

    signal flickingLimit()

    property int lastContentY : 0

    function backToTop(){
        contentContainer.contentY = 0
    }

    function simulateFlick(){

        contentContainer.flick(0, -500)

        if(contentContainer.contentY != root.lastContentY){
            root.lastContentY = contentContainer.contentY
        }else{
            //root.lastContentY = 0
            root.flickingLimit()
        }
    }

    state: Constants.STATE_NOTHING

    Item{
        id: box
        width: topFrame.width
        height: topFrame.height

        Image{
            id: topFrame
            source : assetsPrefix + "Assets/Images/image-textbox-frame-top.png"
            anchors.centerIn: parent
            anchors.verticalCenterOffset : -offset
            sourceSize.width: Constants.TEXTBOX_INNERFRAME_WIDTH * Constants.GLOBAL_SCALE
            sourceSize.height:  Constants.TEXTBOX_INNERFRAME_HEIGHT * Constants.GLOBAL_SCALE
            antialiasing: true

        }

        Image{
            id: bottomFrame
            sourceSize.width: Constants.TEXTBOX_INNERFRAME_WIDTH * Constants.GLOBAL_SCALE
            sourceSize.height:  Constants.TEXTBOX_INNERFRAME_HEIGHT * Constants.GLOBAL_SCALE
            source : assetsPrefix + "Assets/Images/image-textbox-frame-bottom.png"
            anchors.centerIn: parent
            anchors.verticalCenterOffset : offset
            antialiasing: true
        }

        Rectangle{
            id: mask
            anchors.fill: contentContainer

            gradient: Gradient {
                GradientStop { position: 0.0; color: "transparent" }
                GradientStop { position: Constants.TEXT_FADE_FRACTION; color: "white" }
                GradientStop { position: 1.0 - Constants.TEXT_FADE_FRACTION; color: "white" }
                GradientStop { position: 1.0; color: "transparent" }
            }
            antialiasing: true
            visible: false
        }

        OpacityMaskCustom {
            id: op
            source: contentContainer
            maskSource: mask
            anchors.fill:contentContainer
            antialiasing: true
        }

        Flickable{
            id: contentContainer

            visible: true
            antialiasing: true

            opacity : offset > root.textAppearOffset ? 1.0 : 0.0

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: topFrame.top
            anchors.bottom: bottomFrame.bottom
            anchors.margins : root.margins

            contentHeight: content.height + title.height + subtitle.height + 30 + 20 + 20 + 20

            clip : true

            Text{
                id: title
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: 30

                text: root.title
                color: "white"
                font.pointSize: 20
                //font.bold: true
                font.family: Constants.FONT_LIGHT_FAMILY
                wrapMode: Text.WordWrap
                //smooth: !busy
                antialiasing: true
            }

            Text{
                id: subtitle
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: title.bottom
                anchors.topMargin: 14

                text: root.subtitle
                color: "white"
                font.pointSize: 14
                font.family: Constants.FONT_ROMAN_FAMILY
                wrapMode: Text.WordWrap
                //smooth: !busy
                antialiasing: true
            }

            Text{
                id: content
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: subtitle.bottom
                anchors.topMargin: 12

                text: root.text
                color: "white"
                font.pointSize: 12
                font.family: Constants.FONT_ROMAN_FAMILY
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignJustify

                //smooth: !busy
                antialiasing: true
            }

            Behavior on opacity{
                NumberAnimation{}
            }
        }

        Behavior on opacity {
            NumberAnimation{ duration: Constants.ANIMATION_DURATION_SLOW}
        }

        TextBoxScrollBar{
            target: contentContainer
            visible: contentContainer.contentHeight > contentContainer.height
            opacity: contentContainer.moving ? 1.0 : 0.0
            Behavior on opacity{
                NumberAnimation { duration: 250; easing.type: Easing.InOutQuad}
            }
        }

    }

    /*
        bridge
    */
    onFinished: {
    }

    /*
       states and transitions
    */
    states: [
        State{
            name : Constants.STATE_NOTHING
            PropertyChanges { target: box; opacity: 0.0}
        },
        State{
            name : Constants.STATE_SOMETHING
            PropertyChanges { target: box; opacity: 1.0}
        }
    ]

    transitions: [
        Transition{
            from : Constants.STATE_NOTHING; to: Constants.STATE_SOMETHING;

            SequentialAnimation{
                PropertyAction { target: root; property: 'busy'; value: true}
                //BlinkingAnimation{ target: box}
                NumberAnimation{ target: root; property: "offset"; from: root.startOffset; to: root.endOffset; easing.type: Easing.OutSine; duration: 500 }
                //BlinkingAnimation{ target: box}
                PropertyAction { target: root; property: 'busy'; value: false}
                ScriptAction { script: root.finished()}
            }
        },

        Transition{
            from : Constants.STATE_SOMETHING; to: Constants.STATE_NOTHING;

            SequentialAnimation{
                PropertyAction { target: root; property: 'busy'; value: true}
                //BlinkingAnimation{ target: box}
                PauseAnimation {}
                NumberAnimation{ target: root; property: "offset"; from: root.endOffset; to: root.startOffset; easing.type: Easing.OutSine; duration: 500}
                PropertyAction { target: contentContainer; property: "contentY"; value: 0}
                PropertyAction { target: root; property: 'busy'; value: false}
                ScriptAction { script: root.finished()}
            }
        },

        Transition{
            from : Constants.STATE_SOMETHING; to: Constants.STATE_SWITCHLOCALE;

            SequentialAnimation{
                PropertyAction { target: root; property: 'busy'; value: true}

                //BlinkingAnimation{ target: box}
                PauseAnimation {}
                NumberAnimation{ target: root; property: "offset"; from: root.endOffset; to: root.startOffset; easing.type: Easing.OutSine; duration: 200}
                NumberAnimation{ target: contentContainer; property: "opacity"; to: 0.0; easing.type: Easing.OutSine; duration: 50}
                ScriptAction{ script: root.loadLocale()}
                NumberAnimation{ target: contentContainer; property: "opacity"; to: 1.0; easing.type: Easing.OutSine; duration: 50}
                NumberAnimation{ target: root; property: "offset"; from: root.startOffset; to: root.endOffset; easing.type: Easing.OutSine; duration: 200 }
                PropertyAction { target: root; property: 'state'; value: Constants.STATE_SOMETHING}
                PropertyAction { target: root; property: 'busy'; value: false}
                ScriptAction { script: root.finished()}
            }
        }


    ]

}
