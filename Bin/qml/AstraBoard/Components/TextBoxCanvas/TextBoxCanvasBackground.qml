/****************************************************************************
**
** Copyright (C) 2013 Labtek
** All rights reserved.
** Author: Dhi Aurrahman <dio@labtekindie.com>
**
** This file is part of the Museum Astra Project
**
****************************************************************************/

import QtQuick 2.0
import QtGraphicalEffects 1.0
import "../../Components/Utils/Constants.js" as Constants

Item{
    id: root

    width : Constants.TEXTBOX_BACKGROUND_WIDTH * Constants.GLOBAL_SCALE
    height : Constants.TEXTBOX_BACKGROUND_HEIGHT * Constants.GLOBAL_SCALE

    property string assetsPrefix : "../../"
    property string source: assetsPrefix + "Assets/Images/image-textbox-background.png"

    property double angle: 0.0
    property bool busy: false
    signal finished()

    opacity: 0.5

    state: Constants.STATE_NOTHING

    /*
           box, image and mask
    */
    Image{
        id: box
        source: root.source
        visible: false
        sourceSize.width: root.width
        sourceSize.height: root.height
        antialiasing: true
    }

    Image{
        id: image
        source: root.source
        visible: false
        sourceSize.width: root.width
        sourceSize.height: root.height
        antialiasing: true
    }

    Item{
        id: mask
        anchors.fill: parent
        property string color: 'red'
        property int size: 0
        visible: false

        Rectangle{
            id: rect
            width : parent.width
            height : mask.size
            color: mask.color
        }
    }

    /*
           visual: visible element
    */
    OpacityMask{
        id: visual
        smooth: true
        anchors.fill: parent
        source: box
        maskSource: mask
        antialiasing: true
    }

    /*
        bridge
    */
    onFinished: {
    }

    /*
       states and transitions
    */

    states: [
        State{
            name : Constants.STATE_NOTHING
        },
        State{
            name : Constants.STATE_SOMETHING
        }
    ]

    transitions: [
        Transition{
            from : Constants.STATE_NOTHING; to: Constants.STATE_SOMETHING;

            SequentialAnimation{
                PropertyAction { target: root; property: 'busy'; value: true}
                PropertyAction { target: root; property: 'opacity'; value: 1.0}
                ParallelAnimation{
                    NumberAnimation { target: mask; property: 'size'; from: 0; to: parent.height; duration: Constants.ANIMATION_DURATION_SLOW;}
                }
                PropertyAction { target: root; property: 'opacity'; value: 0.5}
                PropertyAction { target: root; property: 'busy'; value: false}
                ScriptAction { script: root.finished()}
            }
        },

        Transition{
            from : Constants.STATE_SOMETHING; to: Constants.STATE_NOTHING;

            SequentialAnimation{
                /*PropertyAction { target: root; property: 'busy'; value: true}
                ParallelAnimation{
                    NumberAnimation { target: mask; property: 'size'; to: 0; from: parent.height; duration: Constants.ANIMATION_DURATION_SLOW;}
                }
                PropertyAction { target: root; property: 'busy'; value: false}*/
                NumberAnimation{target: root; property: 'opacity'; to: 0.0; }
                ScriptAction { script: root.finished()}
            }
        }
    ]
}
