/*
	ScrollBar component for QML Flickable

	Copyright (c) 2010 Gregory Schlomoff - gregory.schlomoff@gmail.com

	This code is released under the MIT license

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*/

/*
	Usage:

	Flickable {
	  id: myFlickable
	  ...
	}
	ScrollBar {
	  target: myFlickable
	}
*/

import QtQuick 2.0
import "../../Components/Utils/Constants.js" as Constants

Image {
    id: img
    property variant target

    antialiasing: true

    source: "images/scrollbar-line.png"
    anchors {top: target.top; bottom: target.bottom; right: target.right; rightMargin: -20; topMargin: 30; bottomMargin: 30}

    Timer {
        property int scrollAmount
        id: timer
        repeat: true
        interval: 20
        onTriggered: {
            target.contentY = Math.max( 0, Math.min( target.contentY + scrollAmount, target.contentHeight - target.height));
        }
    }

    Item{
        id: track
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width:  slider.width

        MouseArea {
            anchors.fill: parent
            onPressed: {
                timer.scrollAmount = target.height * (mouseY < slider.y ? -1 : 1)	// scroll by a page
                timer.running = true;
            }
            onReleased: {
                timer.running = false;
            }
        }

        Rectangle {
            id:slider
            //source: "images/scrollbar-handle.png"
            width: 8
            radius: 4
            anchors.horizontalCenter: parent.horizontalCenter

            property int yCurrent: (target.visibleArea.yPosition) * track.height

            color: Constants.THEME_COLOR

            border.color: "white"

            antialiasing:  true
            height: Math.min(target.height / target.contentHeight * track.height, track.height)
            y: yCurrent < 0 ? 0 : (yCurrent > (img.height - height) ? (img.height - height) : yCurrent)

            MouseArea {
                anchors.fill: parent
                drag.target: parent
                drag.axis: Drag.YAxis
                drag.minimumY: 0
                drag.maximumY: track.height - height

                onPositionChanged: {
                    if (pressedButtons == Qt.LeftButton) {
                        target.contentY = slider.y * target.contentHeight / track.height
                    }
                }
            }
        }
    }
}
