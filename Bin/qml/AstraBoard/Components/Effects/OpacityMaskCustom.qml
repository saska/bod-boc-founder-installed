/****************************************************************************
**
** Copyright (C) 2013 Labtek
** All rights reserved.
** Author: Dhi Aurrahman <dio@labtekindie.com>
**
** This file is part of the Museum Astra Project
**
****************************************************************************/

import QtQuick 2.0
import "internal"

Item {
    id: rootItem
    property variant source
    property variant maskSource
    property bool cached: false

    SourceProxy{
        id: proxyInput
        input: rootItem.source
    }

    SourceProxy{
        id: proxyMask
        input: rootItem.maskSource
    }

    ShaderEffectSource {
        id: sourceProxy
        sourceItem: proxyInput.inputItem
        live: true
        hideSource: true
        smooth: true
    }

    ShaderEffectSource {
        id: maskSourceProxy
        sourceItem: proxyMask.inputItem
        live: true
        hideSource: true
        smooth: true
    }

    /*
    ShaderEffectSource {
        id: cacheItem
        anchors.fill: parent
        visible: rootItem.cached
        smooth: true
        sourceItem: shaderItem
        live: true
        hideSource: visible
    }*/

    ShaderEffect {
        id: shaderItem
        property variant source: sourceProxy
        property variant maskSource: maskSourceProxy

        anchors.fill: parent

        fragmentShader: "
            varying highp vec2 qt_TexCoord0;
            uniform highp float qt_Opacity;
            uniform sampler2D source;
            uniform sampler2D maskSource;
            void main(void) {
                gl_FragColor = texture2D(source, qt_TexCoord0.st) * (texture2D(maskSource, qt_TexCoord0.st).a) * qt_Opacity;
            }
        "
    }

}
