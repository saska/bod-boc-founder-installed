import QtQuick 2.0
import Qt.labs.shaders 1.0

ShaderEffect
{
    id: effect
    property variant image: 0
    property variant mask: 0

    fragmentShader:
        "
        varying highp vec2 qt_TexCoord0;
        uniform sampler2D image;
        uniform sampler2D mask;

        void main(void)
        {
            gl_FragColor = texture2D(image, qt_TexCoord0.st) * (texture2D(mask, qt_TexCoord0.st).a);
        }
        "
}
