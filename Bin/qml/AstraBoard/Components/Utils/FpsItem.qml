import QtQuick 2.0

Item {
    id: root

    signal clicked()

    property int frameCounter: 0
    property int fps: 0;

    width: 48
    height: 48

    Image {
        id: spinnerImage
        source: "../../Assets/Images/spinner.png"
        NumberAnimation on rotation {
            from:0
            to: 360
            duration: 800
            loops: Animation.Infinite
        }
        onRotationChanged: root.frameCounter++;
        anchors.centerIn: parent
    }


    Rectangle{
        width: parent.width
        height: parent.height
        opacity: 0.5
        radius: parent.width/2
        antialiasing: true

        Text {
            anchors.centerIn: parent
            color: "black"
            font.pixelSize: 12
            text: root.fps
        }
    }

    Timer {
        interval: 2000
        repeat: true
        running: true
        onTriggered: {
            root.fps = root.frameCounter/2;
            root.frameCounter = 0;
        }
    }

    MouseArea{
        anchors.fill: parent
        onClicked: {
            root.clicked()
        }
    }
}
