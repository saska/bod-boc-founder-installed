import QtQuick 2.0
import "../Utils/Constants.js" as Constants

Rectangle {
    id: rect
    anchors.top: parent.top
    anchors.topMargin: -offset
    width: parent.width

    opacity: 0.8

    signal hidden()

    height: 40

    property int offset: 40
    property bool hiding : true
    property bool loading: false

    visible: offset < 40

    Text{
        text: "loading..."
        font.pixelSize: 20
        anchors.centerIn: parent
        font.family: Constants.FONT_ROMAN_FAMILY
        color: Constants.THEME_COLOR
    }

    Timer{
        id: delay
        interval: 2500
        onTriggered: {
            rect.offset = 40
            rect.hidden()
            rect.loading = false
        }
    }

    function hide(){
        rect.hiding = true
        delay.start()
    }

    function show(){
        rect.offset = 0
        rect.loading = true
    }

    Behavior on offset{
        SmoothedAnimation{}
    }
}
