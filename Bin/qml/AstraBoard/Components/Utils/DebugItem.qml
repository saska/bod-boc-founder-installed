import QtQuick 2.0

Rectangle {
    anchors.top: parent.top
    anchors.right: parent.right
    height: debugText.height + 10
    width: debugText.width + 10

    border.color: "green"
    color: "black"

    function setText(txt){ debugText.text = txt}
    property alias text: debugText.text

    Text{
        id: debugText
        anchors.centerIn: parent
        width: 200
        color: "white"
        wrapMode: Text.WrapAnywhere
        font.family: "Courier"
    }
}
