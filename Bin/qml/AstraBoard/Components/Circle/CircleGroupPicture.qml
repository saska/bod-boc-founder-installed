import QtQuick 2.0
import QtGraphicalEffects 1.0
import "../Utils/Constants.js" as Constants

Item {
    id: circleGroupPicture
    width: 700
    height: 700

    antialiasing: true
    opacity: mouseArea.pressed ? 0.8 : 1.0

    signal clicked()

    property alias source : box.source

    Item{
        id: mask
        width: 700
        height: 700

        Rectangle{
            anchors.fill: parent
            radius: 350
            antialiasing: true
        }
        visible: false
    }

    Image{
        id: box
        sourceSize.height: 700 * scale
        sourceSize.width: 700 * scale
        visible: false
    }

    OpacityMask{
        id: op
        source: box
        maskSource: mask
        anchors.fill: mask
    }

    Rectangle{
        color: "transparent"
        border.color: "white"
        border.width: 1
        width: box.width
        height: box.height
        radius: width/2
        anchors.centerIn: box
        antialiasing: true
    }


    MouseArea{
        id: mouseArea
        anchors.fill: parent
        onClicked: circleGroupPicture.clicked()
    }
}
