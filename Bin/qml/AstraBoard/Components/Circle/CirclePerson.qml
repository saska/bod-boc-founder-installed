import QtQuick 2.0
import QtGraphicalEffects 1.0
import "../Utils/Constants.js" as Constants

Item {
    id: circlePerson
    width: 406
    height: 406

    antialiasing: true
    opacity: mouseArea.pressed ? 0.8 : 1.0

    visible: opacity > 0

    signal finished()
    signal clicked()

    property alias source : box.source
    property alias text : nameTag.text

    onTextChanged: {
        if(text.toLocaleLowerCase().indexOf('djojohadikusumo') > -1){
            nameTag.font.pixelSize = 26
        }
    }

    Item{
        id: mask
        width: 406
        height: 406

        Rectangle{
            anchors.fill: parent
            radius: 203
            antialiasing: true
        }
        visible: false
    }

    Image{
        id: box
        sourceSize.height: 406 * scale
        sourceSize.width: 406 * scale
        visible: false

        Rectangle{
            id: badge
            anchors.bottom: box.bottom
            //anchors.bottomMargin: 15
            anchors.horizontalCenter: parent.horizontalCenter
            width: box.width
            opacity: 0.8
            height: 90
            visible: nameTag.text.length > 0
        }

        Text{
            id: nameTag
            anchors.top: badge.top
            anchors.topMargin: 0
            anchors.horizontalCenter: parent.horizontalCenter
            width: badge.width - 120
            wrapMode: Text.WordWrap
            font.pixelSize: circlePerson.scale < 1.0 ? 14 : 30
            font.family: Constants.FONT_ROMAN_FAMILY
            color: Constants.THEME_COLOR
            horizontalAlignment: Text.AlignHCenter
            antialiasing: true
            visible: nameTag.text.length > 0
        }
    }

    OpacityMask{
        id: op
        source: box
        maskSource: mask
        anchors.fill: mask
    }

    MouseArea{
        id: mouseArea
        anchors.fill: parent
        enabled: circlePerson.opacity == 1.0
        onClicked: circlePerson.clicked()
    }
}
