import QtQuick 2.0
import QtGraphicalEffects 1.0
import "../../Components/Utils/Constants.js" as Constants

Item {
    id: titleText
    property int counter : 0
    property string text : ""
    property string tempText : ""
    property string tempCopyText : ""

    property alias fontPixelSize: box.font.pixelSize
    property alias fontColor: box.color
    property bool shadowVisible: true

    property bool active: true

    width: box.width
    height: box.height

    Text{
        id: box
        font.family: Constants.FONT_ROMAN_FAMILY
        font.bold: true
    }

    DropShadow{
        id: shadow
        anchors.fill: box
        verticalOffset: 1
        radius: 8.0
        samples: 8
        color: "black"
        source: box
        visible: titleText.shadowVisible
    }

    onTextChanged: {
        if(active){
            if(titleText.state == "nothing" ) {
                tempText = text
                titleText.state = "something"
            }else{
                titleText.state = "nothing"
                delay.start()
            }
        }else{
            box.text = text
        }
    }

    Timer{
        id: delay
        interval: 500
        onTriggered: {
            tempText = text
            titleText.state = "something"
        }
    }

    state: "nothing"

    transitions: [
        Transition {
            from: "nothing"
            to: "something"
            ParallelAnimation{
                NumberAnimation {target: titleText; property: "opacity"; to : 1.0; duration: 500; easing.type: Easing.OutSine}
                NumberAnimation {target: titleText; property: 'counter'; from: 0; to: tempText.length; duration: 500; easing.type: Easing.Linear }
            }

        },
        Transition {
            from: "something"
            to: "nothing"
            ParallelAnimation{
                NumberAnimation {target: titleText; property: "opacity"; to : 0.0; duration: 500; easing.type: Easing.OutSine}
                NumberAnimation {target: titleText; property: 'counter'; from: tempText.length; to: 0; duration: 500; easing.type: Easing.Linear }
            }
        }
    ]

    onCounterChanged: {
        shadow.visible = false

        var str = titleText.tempText
        var strCount = titleText.counter

        var strDisplay = ''

        if (strCount == str.length) {
            strDisplay = str
            shadow.visible = titleText.shadowVisible
        }
        else {
            strDisplay += str.slice(0, strCount/2) + ' ' + str.slice(str.length - strCount/2, str.length)
        }


        box.text = strDisplay
    }
}
