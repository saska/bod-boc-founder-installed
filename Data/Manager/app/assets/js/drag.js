var dragSrcEl = null;

function handlePeopleSequence(){
  viewModel.sequenceIsChanged(true)

  $('#save-sequence').unbind('click')
  $('#save-sequence').click(function(){

    var names = $('.person-item-name')
    var positions = $('.person-item-position')

    var data = []

    for(var i = 0; i < names.length; i++){
      var obj = { name : $(names[i]).val(), position : $(positions[i]).val()}
      data.push(obj)
    }

    $.post('/people/arrange', {
      board : viewModel.currentDataType(),
      year : viewModel.currentSelectedPeriod(),
      data : JSON.stringify(data)
    }, function(res){
      console.log(res)
      viewModel.sequenceIsChanged(false)    
    }).error(function(){
      console.log(err)
    })

    

  })
}

function handleDragStart(e) {
  dragSrcEl = this;
  e.dataTransfer.effectAllowed = 'move';
  e.dataTransfer.setData('text/html', this.innerHTML);
}

function handleDragOver(e) {
  if (e.preventDefault) {
    e.preventDefault(); 
  }

  e.dataTransfer.dropEffect = 'move';  

  return false;
}

function handleDragEnter(e) {
}

function handleDragLeave(e) {
}

function handleDrop(e) {

  if (e.stopPropagation) {
  	e.stopPropagation(); 
  }

  if (dragSrcEl != this) {
	 dragSrcEl.innerHTML = this.innerHTML;
	 this.innerHTML = e.dataTransfer.getData('text/html');
  }

  // todo: sending data to server, current sequence
  handlePeopleSequence()

  return false;
}

function handleDragEnd(e) {
}

function dragClear(){
  var cols = document.querySelectorAll('#people-container .person-item');
  for(var i = 0; i < cols.length; i++){
    var col = cols[i]

    col.removeEventListener('dragstart');
    col.removeEventListener('dragenter')
    col.removeEventListener('dragover');
    col.removeEventListener('dragleave');
    col.removeEventListener('drop');
    col.removeEventListener('dragend');
  }
}


function dragInit(){
	var cols = document.querySelectorAll('#people-container .person-item');
	for(var i = 0; i < cols.length; i++){
		var col = cols[i]

		col.removeEventListener('dragstart');
		col.removeEventListener('dragenter')
		col.removeEventListener('dragover');
		col.removeEventListener('dragleave');
		col.removeEventListener('drop');
		col.removeEventListener('dragend');

		col.addEventListener('dragstart', handleDragStart, false);
		col.addEventListener('dragenter', handleDragEnter, false)
		col.addEventListener('dragover', handleDragOver, false);
		col.addEventListener('dragleave', handleDragLeave, false);
		col.addEventListener('drop', handleDrop, false);
		col.addEventListener('dragend', handleDragEnd, false);
	}
}