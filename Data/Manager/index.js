var express = require('express')
var server = express()
server.use('/', express.static(__dirname + "/app"))
server.use('/temp', express.static(__dirname + "/temp"))
server.use('/Contents', express.static(__dirname + "/../Contents"))
server.use(express.bodyParser({keepExtensions : true, uploadDir: __dirname + '/temp'}))
var routes = require(__dirname + "/astraboard/routes")(server)
server.listen(3000)

