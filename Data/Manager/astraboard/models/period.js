var fs = require('fs.extra')
var _ = require('lodash')
var moment = require('moment')

var PeriodTitle = {
  id_ID : {
    Founder : 'Pendiri',
    BOC : 'Dewan Komisaris',
    BOD : 'Dewan Direksi',
    Test : 'Judul Tes'
  },
  en_US : {
    Founder : 'Founder',
    BOC : 'Board of Commissioners',
    BOD : 'Board of Directors',
    Test : 'Test Title' 
  }
}

function _parseDateFromInputIsValid(date){
  if(!date) return false
  moment.lang('en')

  var d = date + ''

  if(d.indexOf(' ') > -1){
    return moment(d, 'MMMM YYYY').isValid()
  }else{
    if(parseInt(d)){
      return ((parseInt(d) > 1950) && moment(d, "YYYY"))
    }else{
      return false
    }
  }
}

function _toLang(date, lang){
  if(!_parseDateFromInputIsValid(date)) return 'invalid date'
  var m = moment(date)
  if(date.indexOf(' ') > -1){
    m.lang(lang)
    return m.format('MMMM YYYY')
  }else{
    return date
  }
}

function _toFolderName(date){
  if(!_parseDateFromInputIsValid(date)) return 'invalid date'
  var m = moment(date)
  if(date.indexOf(' ') > -1){
    m.lang('id')
    return m.format('YYYY MM-MMMM') 
  }else{
    return date
  }
}

function _exists(board, name, cb){
  var rootPath = __dirname + '/../../../Contents/' + board + '/People';
  var str_en_US = fs.readFileSync(rootPath + '/en_US.json')
  var obj_en_US = JSON.parse(str_en_US)
  cb(null, (_.indexOf(obj_en_US.years, name) > -1))
}

function _validDate(date){
  return _parseDateFromInputIsValid(date)
}

function _create(board, options, cb){
  var rootPath = __dirname + '/../../../Contents/' + board + '/People';
  var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
  var str_en_US = fs.readFileSync(rootPath + '/en_US.json')

  var obj_id_ID = JSON.parse(str_id_ID)
  var obj_en_US = JSON.parse(str_en_US)
}

exports.meta = function(board, options, cb){

  var rootPath = __dirname + '/../../../Contents/' + board + '/People'

  if(options.year){
    if(! _validDate(options.year)) return cb({ error: true, message: 'Invalid date'})
      
    _exists(board, options.year, function(err, ex){
      if(ex){
        rootPath += '/' +  _toFolderName(options.year);
        var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
        var str_en_US = fs.readFileSync(rootPath + '/en_US.json')
        cb(null, {
          en_US : JSON.parse(str_en_US),
          id_ID : JSON.parse(str_id_ID)
        })
      }
      else{
        cb({error : true, message: "Not found"})
      }
    })

  }
  else if(options.folderName){
    rootPath += '/' +  options.folderName;
    var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
    var str_en_US = fs.readFileSync(rootPath + '/en_US.json')
    cb(null, {
      en_US : JSON.parse(str_en_US),
      id_ID : JSON.parse(str_id_ID)
    })
  }

}

exports.list = function(board, cb){
  var rootPath = __dirname + '/../../../Contents/' + board + '/People';
  var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
  var str_en_US = fs.readFileSync(rootPath + '/en_US.json')
  var folderNames = _.without(fs.readdirSync(rootPath), '.DS_Store')

  var obj_id_ID = JSON.parse(str_id_ID)
  var obj_en_US = JSON.parse(str_en_US)

  var meta = {
    title : {
      en_US : obj_en_US.title,
      id_ID : obj_id_ID.title
    },
    menuTitle : {
      en_US : obj_en_US.menuTitle,
      id_ID : obj_id_ID.menuTitle
    }
  }

  var res = {
    meta : meta,
    data : []
  }

  for(var i = 0; i < obj_en_US.years.length; i++){
    var obj = { en_US : obj_en_US.years[i], id_ID : obj_id_ID.years[i], folderName : folderNames[i]}
    res.data.push(obj)
  }

  cb(null, res)
}

exports.remove = function(board, options, cb){
  if(! _validDate(options.year)) return cb({ error: true, message: 'Invalid date'})

  _exists(board, options.year, function(err, ex){

    if(ex){
      var rootPath = __dirname + '/../../../Contents/' + board + '/People';
      var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
      var str_en_US = fs.readFileSync(rootPath + '/en_US.json')

      var obj_en_US = JSON.parse(str_en_US)
      var obj_id_ID = JSON.parse(str_id_ID)

      var idx = _.indexOf(obj_en_US.years, options.year)
      obj_en_US.years = _.without(obj_en_US.years, options.year)
      obj_id_ID.years = _.without(obj_id_ID.years, obj_id_ID.years[idx])

      var obj = {
        root : {
          en_US : obj_en_US,
          id_ID : obj_id_ID
        },
        folderName : _toFolderName(options.year)
      }

      fs.rmrfSync(rootPath + '/' + obj.folderName)
      fs.writeFileSync(rootPath + '/id_ID.json', JSON.stringify(obj.root.id_ID, undefined, 2))
      fs.writeFileSync(rootPath + '/en_US.json', JSON.stringify(obj.root.en_US, undefined, 2))
      
      cb(null, obj)
    }
    else return cb({ error: true, message: 'Not found'})
  })

}

exports.create = function(board, options, cb){
  if(! _validDate(options.year)) return cb({ error: true, message: 'Invalid date'})

  _exists(board, options.year, function(err, ex){
    if(ex){
      return cb({ error : true, message: 'Already exists'})
    }else{
      var rootPath = __dirname + '/../../../Contents/' + board + '/People';
      var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
      var str_en_US = fs.readFileSync(rootPath + '/en_US.json')

      var period = {
        title_en_US : PeriodTitle.en_US[board],
        title_id_ID : PeriodTitle.id_ID[board],
        positions : [ 'Primary', 'Secondary'],
        posisi : [ 'Primer', 'Sekunder'],
        year : options.year
      }

      var obj_en_US = JSON.parse(str_en_US)
      var obj_id_ID = JSON.parse(str_id_ID)
      
      obj_en_US.years.push(period.year)
      obj_en_US.years = _.sortBy(obj_en_US.years, function(year){ return moment(year)})

      var idx = obj_en_US.years.indexOf(period.year)

      var a = _.first(obj_id_ID.years, idx)
      var b = _.last(obj_id_ID.years, obj_id_ID.years.length - idx)

      a.push(_toLang(period.year, 'id'))
      obj_id_ID.years = _.union(a, b)

      var obj = {
        root : {
          en_US : obj_en_US,
          id_ID : obj_id_ID
        },
        content : {
          en_US : {
            year : period.year,
            title : period.title_en_US,
            positions : period.positions
          },
          id_ID : {
            year : _toLang(period.year, 'id'),
            title : period.title_id_ID,
            positions : period.posisi
          }
        },
        folderName : _toFolderName(period.year)
      }

      // write to disk
      fs.mkdirpSync(rootPath + '/' + obj.folderName + '/0')
      fs.mkdirpSync(rootPath + '/' + obj.folderName + '/1')
      fs.mkdirpSync(rootPath + '/' + obj.folderName + '/text')

      fs.writeFileSync(rootPath + '/' + obj.folderName + '/id_ID.json', JSON.stringify(obj.content.id_ID, undefined, 2))
      fs.writeFileSync(rootPath + '/' + obj.folderName + '/en_US.json', JSON.stringify(obj.content.en_US, undefined, 2))
      fs.writeFileSync(rootPath + '/id_ID.json', JSON.stringify(obj.root.id_ID, undefined, 2))
      fs.writeFileSync(rootPath + '/en_US.json', JSON.stringify(obj.root.en_US, undefined, 2))


      cb(null, obj)
    }
  })
}

exports.exists = function(board, name, cb){
  _exists(board, name, cb)
}

exports.normalize = function(board, year, cb){

  var folderName = _toFolderName(year)
  var rootPath = __dirname + '/../../../Contents/' + board + '/People/' + folderName;
  var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
  var str_en_US = fs.readFileSync(rootPath + '/en_US.json')

  var obj_id_ID = JSON.parse(str_id_ID)
  var obj_en_US = JSON.parse(str_en_US)

  var positionsLen = obj_id_ID.positions.length
  var tobeMoved = []

  for(var i = 0; i < positionsLen; i++){
    var files = fs.readdirSync(rootPath + '/' + i)
    files = _.without(files, ".DS_Store")

    for(var j = 0; j < files.length; j++){
      var file = files[j]

      var src = rootPath + '/' + i + '/' + file
      var dst = rootPath + '/1/' + file 

      if(file.indexOf('_') > -1){
          file = file.substr(file.indexOf('_') + 1)
      }

      if(i > 1) {
        dst = rootPath + '/' + 1 + '/' + file 
        tobeMoved.push({ src : src, dst : dst})
      }
      else{

        if(files[j].indexOf('_') > -1){
          dst = rootPath + '/' + i + '/' + file 
          tobeMoved.push({ src : src, dst : dst})
        }

      }
    }
  }

  var count = 0
  for(var k = 0; k < tobeMoved.length; k++){
    var act = tobeMoved[k]
    fs.move(act.src, act.dst, function(err){
      if(err) console.log(err)
      count++
      if(count == tobeMoved.length){
        obj_id_ID.positions = ['Primer', 'Sekunder']
        obj_en_US.positions = ['Primary', 'Secondary']
        fs.writeFileSync(rootPath + '/id_ID.json', JSON.stringify(obj_id_ID, undefined, 2))
        fs.writeFileSync(rootPath + '/en_US.json', JSON.stringify(obj_en_US, undefined, 2))
        for(var m = 2 ; m < positionsLen; m++) fs.rmrfSync(rootPath + '/' + m)
        cb(null, {})
      }
    })
  }

  if(tobeMoved.length == 0) {
    obj_id_ID.positions = ['Primer', 'Sekunder']
    obj_en_US.positions = ['Primary', 'Secondary']
    fs.writeFileSync(rootPath + '/id_ID.json', JSON.stringify(obj_id_ID, undefined, 2))
    fs.writeFileSync(rootPath + '/en_US.json', JSON.stringify(obj_en_US, undefined, 2))
    for(var m = 2 ; m < positionsLen; m++) fs.rmrfSync(rootPath + '/' + m)
    cb(null, {})
  }

}
