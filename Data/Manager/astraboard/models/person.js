var fs = require('fs.extra')
var _ = require('lodash')
var moment = require('moment')

function _normalize(board, year, cb){
  var folderName = _toFolderName(year)
  var rootPath = __dirname + '/../../../Contents/' + board + '/People/' + folderName;
  var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
  var str_en_US = fs.readFileSync(rootPath + '/en_US.json')

  var obj_id_ID = JSON.parse(str_id_ID)
  var obj_en_US = JSON.parse(str_en_US)

  var positionsLen = obj_id_ID.positions.length
  var tobeMoved = []

  for(var i = 0; i < positionsLen; i++){
    var files = fs.readdirSync(rootPath + '/' + i)
    files = _.without(files, ".DS_Store")

    for(var j = 0; j < files.length; j++){
      var file = files[j]

      var src = rootPath + '/' + i + '/' + file
      var dst = rootPath + '/1/' + file 

      if(file.indexOf('_') > -1){
          file = file.substr(file.indexOf('_') + 1)
      }

      if(i > 1) {
        dst = rootPath + '/' + 1 + '/' + file 
        tobeMoved.push({ src : src, dst : dst})
      }
      else{

        if(files[j].indexOf('_') > -1){
          dst = rootPath + '/' + i + '/' + file 
          tobeMoved.push({ src : src, dst : dst})
        }

      }
    }
  }

  var count = 0
  for(var k = 0; k < tobeMoved.length; k++){
    var act = tobeMoved[k]
    fs.move(act.src, act.dst, function(err){
      if(err) console.log(err)
      count++
      if(count == tobeMoved.length){
        obj_id_ID.positions = ['Primer', 'Sekunder']
        obj_en_US.positions = ['Primary', 'Secondary']
        fs.writeFileSync(rootPath + '/id_ID.json', JSON.stringify(obj_id_ID, undefined, 2))
        fs.writeFileSync(rootPath + '/en_US.json', JSON.stringify(obj_en_US, undefined, 2))
        for(var m = 2 ; m < positionsLen; m++) fs.rmrfSync(rootPath + '/' + m)
        cb(null, {})
      }
    })
  }

  if(tobeMoved.length == 0) {
    obj_id_ID.positions = ['Primer', 'Sekunder']
    obj_en_US.positions = ['Primary', 'Secondary']
    fs.writeFileSync(rootPath + '/id_ID.json', JSON.stringify(obj_id_ID, undefined, 2))
    fs.writeFileSync(rootPath + '/en_US.json', JSON.stringify(obj_en_US, undefined, 2))
    for(var m = 2 ; m < positionsLen; m++) fs.rmrfSync(rootPath + '/' + m)
    cb(null, {})
  }
}

function _parseDateFromInputIsValid(date){
  if(!date) return false

  moment.lang('en')

  var d = date + ''

  if(d.indexOf(' ') > -1){
    return moment(d, 'MMMM YYYY').isValid()
  }else{
    if(parseInt(d)){
      return ((parseInt(d) > 1950) && moment(d, "YYYY"))
    }else{
      return false
    }
  }
}

function _toLang(date, lang){
  if(!_parseDateFromInputIsValid(date)) return 'invalid date'
  var m = moment(date)
  if(date.indexOf(' ') > -1){
    m.lang(lang)
    return m.format('MMMM YYYY')
  }else{
    return date
  }
}

function _toFolderName(date){
  if(date == 0) return date
  if(!_parseDateFromInputIsValid(date)) return 'invalid date'
  var m = moment(date)
  if(date.indexOf(' ') > -1){
    m.lang('id')
    return m.format('YYYY MM-MMMM') 
  }else{
    return date
  }
}

function _exists(board, name, cb){
  var rootPath = __dirname + '/../../../Contents/' + board + '/People';
  var str_en_US = fs.readFileSync(rootPath + '/en_US.json')
  var obj_en_US = JSON.parse(str_en_US)
  cb(null, (_.indexOf(obj_en_US.years, name) > -1))
}

function _validDate(date){
  if(date == 0) return true
  return _parseDateFromInputIsValid(date)
}

function _pad(num, size) { var s = "000000000" + num; return s.substr(s.length-size);}

function _list (board, year,cb){
  if(!_validDate(year)) return cb({ error : true, message: 'Invalid date'})
  _exists(board, year, function(err, ex){
    if(ex){
      var folderName = board != 'Founder' ? _toFolderName(year) : '0'
      var rootPath = __dirname + '/../../../Contents/' + board + '/People/' + folderName; 

      var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
      var str_en_US = fs.readFileSync(rootPath + '/en_US.json')

      var obj_en_US = JSON.parse(str_en_US)
      var obj_id_ID = JSON.parse(str_id_ID)

      var people = []
      var personIdx = 0

      for(var i = 0; i < obj_en_US.positions.length; i++){
        
        var arr = fs.readdirSync(rootPath + '/' + i)

        var tempArr = []
        var arranged = false

        for(var j = 0; j < arr.length; j++){
          var str = arr[j]
          var name = str.substring(0, str.lastIndexOf('.'))
          var shortName = ''
          if(str.indexOf('_') > -1){
            if(!arranged) arranged = true
            name = str.substring(str.indexOf('_') + 1, str.lastIndexOf('.'))
            if(obj_en_US.shortNames){
              shortName = obj_en_US.shortNames[name] || ''
            }
          }

          var person = { position : i, index : personIdx++, name : name, shortName : shortName, image : str}
          people.push(person)
        }
      }

      var obj = {
        folderName : folderName,
        arranged : arranged,
        people : people
      }

      cb(null, obj)
    }
    else{
      return cb({ error : true, message: 'Not found'})
    }
  })
}

exports.list = function(board, year,cb){
  _list(board, year, cb)
}

exports.add = function(board, year, options, cb){

  if(!_validDate(year)) return cb({ error : true, message: 'Invalid date'})
  _exists(board, year, function(err, ex){
    if(ex){

      if(!options.name) return cb({ error: true, message: 'Name is required'})
      if(!options.lang) return cb({ error: true, message: 'Lang is required'})

      _list(board, year, function(err, p){
        for(var i = 0; i < p.people.length; i++){
          var person = p.people[i]
          if(person.name == options.name) return cb({ error : true, message : 'Already exist'})
        }

        //write to disk

        var textObj = {
          name : options.name,
          en_US : {
            title : options.name,
            subtitle : "",
            text : ""
          },
          id_ID : {
            title : options.name,
            subtitle : "",
            text : ""
          }
        }

        var folderName = board != 'Founder' ? _toFolderName(year) : '0'
        var rootPath = __dirname + '/../../../Contents/' + board + '/People/' + folderName; 

        fs.writeFileSync(rootPath + '/text/' + options.name + '.json', JSON.stringify(textObj, undefined, 2))
        var src = __dirname +  "/../../app/assets/img/default.jpg"
        var dst = rootPath + "/" + (options.primary == "false" ? '1' : '0') + "/" + options.name + ".jpg"
        
        fs.copy(src, dst, function(err){
          cb(err, {
            name : options.name,
            lang : options.lang,
            title : options.title || options.name,
            subtitle : options.subtitle || '',
            text : options.text || '',
            shortName : options.shortName || ''
          })

        })
      })
    }
    else{
      return cb({ error : true, message: 'Not found'})
    }
  })
}

exports.remove = function(board, year, options, cb){


  if(!_validDate(year)) return cb({ error : true, message: 'Invalid date'})
  _exists(board, year, function(err, ex){
    if(ex){
      _list(board, year, function(err, p){

        for(var i = 0; i < p.people.length; i++){
          var person = p.people[i]
          if(person.name == options.name) {

            var folderName = board != 'Founder' ? _toFolderName(year) : '0'
            var rootPathImage = __dirname + '/../../..'
            var rootPath = __dirname + '/../../../Contents/' + board + '/People/' + folderName; 

            // write to disk
            fs.rmrfSync(rootPathImage + options.image)
            fs.rmrfSync(rootPath + '/text/' +  options.name + '.json')

            // check if the last copy of image in a folder, if it is delete the folder as well
            // check the positions array, delete associated element
            // re-arrange the folder
            // done

            return cb(null, { name : options.name})
          }
        }
        return cb({ error : true, message: 'Not found'})

      })
    }
    else{
      return cb({ error : true, message: 'Not found'})
    }
  })
}

exports.addPicture = function(board, year, options, cb){

}

exports.modifyPicture = function(boadr, year, options, cb){

}

exports.modifyText = function(board, year, options, cb){
  if(!_validDate(year)) return cb({ error : true, message: 'Invalid date'})

  _exists(board, year, function(err, ex){
    if(ex){

      var folderName = _toFolderName(year)
      var rootPath = __dirname + '/../../../Contents/' + board + '/People/' + folderName; 
      var shortName = ''

      var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
      var str_en_US = fs.readFileSync(rootPath + '/en_US.json')
      var text = fs.readFile(rootPath + '/text/' + options.name + '.json', function(err, data){

        if(err) return cb(err)

        var textObj = JSON.parse(data)
        var obj_en_US = JSON.parse(str_en_US)
        var obj_id_ID = JSON.parse(str_id_ID)

        if(options.shortname){
          if(obj_en_US.shortNames){
            shortName = obj_en_US.shortNames[options.name] || ''
          }
        }

        if(options.text) textObj[options.lang].text = options.text
        if(options.title) textObj[options.lang].title = options.title  
        if(options.subtitle) textObj[options.lang].subtitle = options.subtitle

        var obj = {
          name : options.name,
          shortname : options.shortname,
          text : textObj
        }

        if(obj.shortname){
          if(!obj_en_US.shortNames){
            obj_en_US.shortNames = {}
            obj_id_ID.shortNames = {}
          }

          var key = options.name
          obj_en_US.shortNames[key] = obj.shortname
          obj_id_ID.shortNames[key] = obj.shortname
        }

        // write to disk 
        fs.writeFileSync(rootPath + '/text/' + options.name + '.json', JSON.stringify(obj.text, undefined, 2))
        fs.writeFileSync(rootPath + '/id_ID.json', JSON.stringify(obj_id_ID, undefined, 2))
        fs.writeFileSync(rootPath + '/en_US.json', JSON.stringify(obj_en_US, undefined, 2))


        cb(null, obj)

      })
    }
    else{
      return cb({ error : true, message: 'Not found'})
    }
  })
}

exports.show = function(board, year, name, cb){
  if(!_validDate(year)) return cb({ error : true, message: 'Invalid date'})
  _exists(board, year, function(err, ex){
    if(ex){
      var folderName = _toFolderName(year)
      var rootPath = __dirname + '/../../../Contents/' + board + '/People/' + folderName; 

      var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
      var str_en_US = fs.readFileSync(rootPath + '/en_US.json')
      var text = fs.readFile(rootPath + '/text/' + name + '.json', function(err, data){
        if(err) return cb(err)

        var textObj = JSON.parse(data)
        var obj_en_US = JSON.parse(str_en_US)
        var obj_id_ID = JSON.parse(str_id_ID)

        var shortName = ''
        if(obj_en_US.shortNames){
          shortName = obj_en_US.shortNames[name] || ''
        }

        var person = {
          name : name,
          shortName : shortName,
          text : textObj
        }

        cb(null, person)
      })
    }
    else{
      return cb({ error : true, message: 'Not found'})
    }
  })

}

exports.arrangePeople = function(board, year, data, cb){

  _normalize(board, year, function(err, ret){
    if(err) return cb(err)
    else{

      var arr = JSON.parse(data)
      var folderName = _toFolderName(year)
      var imgs =  []
      var rootPath = __dirname + '/../../../Contents/' + board + '/People/' + folderName;
      
      for(var j = 0; j < arr.length; j++){
        var person = arr[j]
        var newPos = j > 0 ? 1 : 0
        var src = rootPath + '/' + person.position + '/' + person.name + '.jpg' 
        var dst = rootPath + '/' + newPos + '/' + _pad(j, 3) + '_' + person.name + '.jpg' 
        fs.renameSync(src, dst)
      }

      cb(null, {})

    }
  })
}

exports.imageSave = function(board, year, name, path, cb){
  if(!_validDate(year)) return cb({ error : true, message: 'Invalid date'})
  _exists(board, year, function(err, ex){
    if(ex){
      var folderName = _toFolderName(year)
      var rootPath = __dirname + '/../../../Contents/' + board + '/People/' + folderName;
      var tempPath = __dirname + '/../..' + path;

      var str_id_ID = fs.readFileSync(rootPath + '/id_ID.json')
      var obj_id_ID = JSON.parse(str_id_ID)

      var fileToBeReplaced = ''
      var index = 0

      for(var i = 0; i < obj_id_ID.positions.length; i++){
        var files = fs.readdirSync(rootPath + '/' + i)
        for(var j = 0; j < files.length; j++){
          var file = files[j]
          if(file.indexOf(name) > -1){
            fileToBeReplaced = file
            index = i
            break;
          }
        }
        if(fileToBeReplaced) break;  
      }

      if(fileToBeReplaced){
        var src = tempPath
        var dst = rootPath + '/' + index + '/' + fileToBeReplaced
        fs.rmrfSync(dst)
        fs.move(src, dst, function(err){
          cb(err, {})
        })
      }

    }
    else{
      return cb({ error : true, message: 'Not found'})
    }
  })
}

