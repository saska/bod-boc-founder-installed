module.exports = function(server){
  
  var Board = require(__dirname + "/../models/board")
  var Period = require(__dirname + "/../models/period")
  var Person = require(__dirname + "/../models/person")
  var Title = require(__dirname + "/../models/title")
  var MenuTitle = require(__dirname + "/../models/menutitle")

  var Utility = require('./utility')

  function wrap(err, data, res){
    if(err){
        var message = err.message || "Unknown error"
        res.send(403, message)
      }else{
        res.send(Utility.wrap('success', data))
      }
  }

  var boards = function(req, res, next){
    Board.list(function(err, data){
      wrap(err, data, res)
    })
  }

  var boardMeta = function(req, res, next){
    Board.meta(req.query.board, function(err, data){
      wrap(err, data, res)
    })
  }

  var periods = function(req, res, next){
    Period.list(req.query.board, function(err, data){
      wrap(err, data, res)
    })
  }

  var showPeriod = function(req, res, next){
    Period.meta(req.query.board, { folderName : req.query.folderName, year : req.query.year}, function(err, data){
      wrap(err, data, res)
    })
  }

  var addPeriod = function(req, res, next){
    Period.create(req.body.board, { year : req.body.year }, function(err, data){
      wrap(err, data, res)
    })
  }

  var removePeriod = function(req, res, next){
    Period.remove(req.body.board, { year : req.body.year }, function(err, data){
      wrap(err, data, res)
    })
  }

  var editPeriod = function(req, res, next){
    
  }

  var people = function(req, res, next){
    Person.list(req.query.board, req.query.year, function(err, data){
      wrap(err, data, res)
    })
  }

  var showPerson = function(req, res, next){
    Person.show(req.query.board, req.query.year, req.query.name, function(err, data){
      wrap(err, data, res)
    })
  }

  var addPerson = function(req, res, next){
    Person.add(req.body.board, req.body.year, 
      {name : req.body.name, lang : 'en_US', primary : req.body.primary },
      function(err, data){
        wrap(err, data, res)
      })
  }

  var removePerson = function(req, res, next){
    Person.remove(req.body.board, req.body.year, { 
      name : req.body.name, 
      position : req.body.position, 
      index : req.body.index,
      image : req.body.image
    }, 
    function(err, data){
      wrap(err, data, res)
    })
  }

  var editPerson = function(req, res, next){
    Person.modifyText(req.body.board, req.body.year, {
      lang : req.body.lang,
      title : req.body.title,
      subtitle : req.body.subtitle,
      text : req.body.text,
      shortname : req.body.shortname,
      name : req.body.name
    },function(err, data){
      wrap(err, data, res)
    })
  }

  var image = function(req, res, next){
    var path = req.files.file.path
    if(path.substr(path.lastIndexOf('.')) != '.jpg'){
      res.send(403, { error : true, message : 'error'})  
    }else{
      res.send({ path : path.substr(path.lastIndexOf('\\'))})
    }
  }

  var imageSave = function(req, res, next){
    Person.imageSave(req.body.board, req.body.year, req.body.name, req.body.path, function(err, data){
      wrap(err, data, res)
    })
  }

  var arrangePeople = function(req, res, next){
    Person.arrangePeople(req.body.board, req.body.year, req.body.data, function(err, data){
      wrap(err, data, res)
    })
  }

  var editTitle = function(req, res, next){
    Title.edit(req.body.board, req.body.en, req.body.id, function(err, data){
      wrap(err, data, res)
    })
  }

  var editMenuTitle = function(req, res, next){
    MenuTitle.edit(req.body.board, req.body.en, req.body.id, function(err, data){
      wrap(err, data, res)
    })
  }

  return {
    boards : boards,
    periods : periods,
    boardMeta : boardMeta,
    showPeriod : showPeriod,
    addPeriod : addPeriod,
    removePeriod : removePeriod,
    editPeriod : editPeriod,
    people : people,
    showPerson : showPerson,
    addPerson : addPerson,
    removePerson : removePerson,
    editPerson : editPerson,
    image : image,
    imageSave : imageSave,
    arrangePeople : arrangePeople,
    editTitle : editTitle,
    editMenuTitle : editMenuTitle
  }
}