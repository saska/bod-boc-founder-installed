var Service = require('node-windows').Service;

var svc = new Service({
  name:'AstraBoard Manager',
  description: 'This is the AstraBoard Manager',
  script: 'index.js',
  env:{
    name: "NODE_PATH",
    value: "C:\\App\\Data\\Manager\\node_modules"
  }
});

svc.on('install',function(){
  svc.start();
});

svc.on('alreadyinstalled',function(){
  console.log('This service is already installed.');
});

svc.on('start',function(){
  console.log(svc.name + ' started!\nVisit http://127.0.0.1:3000 to see it in action.');
});

svc.install();
