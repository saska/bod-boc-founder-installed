var test = require('tape')
var Board = require('../astraboard/models/board')

test('Boards', function(t){
  t.plan(1)
  Board.list(function(err, boards){
    t.ok(boards.length == 3, 'there are only three kind of boards')
    t.end()
  })
})

