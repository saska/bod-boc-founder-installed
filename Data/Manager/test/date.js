var test = require('tape')
var moment = require('moment')
var _ = require('lodash')

function parseDateFromInput(date){
  if(date.indexOf(' ') > -1){
    return moment(date, 'MMMM YYYY').isValid()
  }else{
    if(parseInt(date)){
      return ((parseInt(date) > 1950) && moment(date, "YYYY"))
    }else{
      return false
    }
  }
}

function toLang(date, lang){
  if(!parseDateFromInput(date)) return 'invalid date'
  var m = moment(date).lang(lang)
  if(date.indexOf(' ') > -1){
    return m.format('MMMM YYYY')
  }else{
    return m.format('YYYY')
  }
}

function sortPeriod(){
  var arr = [ 'August 1993', 'December 1993', '1996', '1997']
  var a = 'August 1994'
  arr.push(a)
  return _.sortBy(arr, function(d){ return moment(d)})
}

function splitAndMerge(){
  var arr = [ 'August 1993', 'December 1993', '1996', '1997']
  var a = _.first(arr, 2)
  var b = _.last(arr, arr.length - 2)
}

test('Date', function(t){
  t.plan(7)
  t.ok(parseDateFromInput('August 2010'), 'parse August 2010')
  t.ok(parseDateFromInput('2010'), 'parse 2010')
  t.notOk(parseDateFromInput('aaaa'), 'parse `aaaa` failed')
  t.notOk(parseDateFromInput('201'), 'parse `201` failed')
  t.equal('Agustus 2010', toLang('August 2010', 'id'))
  t.equal('2010', toLang('2010', 'id'))
  t.equal(_.indexOf(sortPeriod(), 'August 1994'), 2)


})