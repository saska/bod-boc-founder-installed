var test = require('tape')
var _ = require('lodash')

var Period = require('../astraboard/models/period')

var head = {
    title : {
      en_US : "Board of Test",
      id_ID : "Dewan Tes",
    },
    year : '1960'
  }

var middle = {
    title : {
      en_US : "Board of Test",
      id_ID : "Dewan Tes",
    },
    year : 'August 1994'
  }

var tail = {
    title : {
      en_US : "Board of Test",
      id_ID : "Dewan Tes",
    },
    year : '1998'
  }

test('Periods', function(t){
  
  t.plan(12)

  Period.list('Test', function(err, periods){
    t.equal(periods.length, 4)
  })

  Period.meta('Test', function(err, meta){
    t.equal(Object.keys(meta).length, 2)
    t.ok(_.has(meta, 'id_ID'), 'has `id_ID` metadata')
    t.ok(_.has(meta, 'en_US'), 'has `en_US` metadata')
    t.ok(meta['en_US'].title == 'Test Title', '`Test Title` as title in en_US metadata')
    t.ok(meta['id_ID'].title == 'Tes Judul', '`Test Judul` as title in id_ID metadata')
  })

  Period.exists('Test', 'August 1993', function(err, ex){
    t.ok(ex, '`August 1993` exists')
  })

  Period.remove('Test', { year : '1997'}, function(err, res){
    t.ok(!err, 'remove a year')
  })

  Period.create('Test', head, 
  function(err, res){
    t.ok(!err, 'created new period at head')
    Period.create('Test', middle, function(err, res){
      t.ok(!err, 'created new period in the middle')
      t.ok(res.folderName == '1994 08-Agustus', 'created 1994 08-Agustus folder name')
      Period.create('Test', tail, function(err, res){
        t.ok((_.indexOf(res.root.id_ID.years, '1998') == (res.root.id_ID.years.length - 1)), 'created new period at tail')
      })
    })
  })
})

